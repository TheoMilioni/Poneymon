package fr.univ_lyon1.groupen.poneymon;

import fr.univ_lyon1.groupen.poneymon.controller.StateOrchestrator;
import fr.univ_lyon1.groupen.poneymon.state.StateDefinition;
import fr.univ_lyon1.groupen.poneymon.view.StateFrame;
import javafx.application.Application;
import javafx.stage.Stage;

import java.security.AccessControlException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Classe principale de l'application.
 *
 * S'appuie sur JavaFX pour le rendu.
 */
public class App extends Application {
    private static final Logger LOGGER = Logger.getLogger(App.class.getName());

    /** Point d'entrée principal pour JavaFX.
     */
    @Override
    public void start(Stage stage) {
        // Nom de la fenetre
        stage.setTitle("Poneymon");

        StateOrchestrator orchestrator = new StateOrchestrator();
        StateFrame frame = new StateFrame(stage);
        orchestrator.setFrame(frame);

        orchestrator.setState(StateDefinition.MAIN_MENU);

        stage.show();
    }

    /**
     * Point d'entrée principal de l'application.
     * @param args Paramètres passés par la JVM
     */
    public static void main(String[] args) {
        try {
            System.setProperty("javafx.animation.fullspeed", "false");
            System.setProperty("javafx.animation.framerate", "60");
            System.setProperty("javafx.animation.pulse", "60");
            System.setProperty("prism.vsync", "true");
        } catch (AccessControlException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }

        Application.launch(args);
    }
}
