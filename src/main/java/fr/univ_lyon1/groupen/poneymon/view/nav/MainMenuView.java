package fr.univ_lyon1.groupen.poneymon.view.nav;

import fr.univ_lyon1.groupen.poneymon.view.StateView;
import javafx.animation.Interpolator;
import javafx.animation.RotateTransition;
import javafx.animation.ScaleTransition;
import javafx.animation.Timeline;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.util.Duration;

import java.io.IOException;

public class MainMenuView implements StateView {
    private Parent root;

    private Button btnSingleplayer;
    private Button btnMultiplayer;
    private Button btnLeaderboard;
    private Button btnExit;

    /**
     * Constructeur de la vue du menu principal.
     */
    public MainMenuView() throws IOException {

        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/fxml/MainMenu.fxml"));
        root = loader.load();
        btnSingleplayer = (Button)loader.getNamespace().get("button_solo");
        btnMultiplayer = (Button)loader.getNamespace().get("button_multi");
        btnLeaderboard = (Button)loader.getNamespace().get("button_leaderboard");
        btnExit = (Button)loader.getNamespace().get("button_exit");

        final Node logo = (Node) loader.getNamespace().get("logo");
        RotateTransition rt = new RotateTransition(Duration.millis(3000), logo);
        rt.setFromAngle(0);
        rt.setToAngle(6);
        rt.setCycleCount(Timeline.INDEFINITE);
        rt.setInterpolator(Interpolator.EASE_BOTH);
        rt.setAutoReverse(true);
        rt.play();
        ScaleTransition st = new ScaleTransition(Duration.millis(3000), logo);
        st.setFromX(1);
        st.setToX(0.9);
        st.setFromY(1);
        st.setToY(0.9);
        st.setCycleCount(Timeline.INDEFINITE);
        st.setInterpolator(Interpolator.EASE_BOTH);
        st.setAutoReverse(true);
        st.play();
    }

    @Override
    public Scene getScene() {
        return root.getScene();
    }

    @Override
    public Node getNode() {
        return root;
    }

    public Button getBtnSingleplayer() {
        return btnSingleplayer;
    }

    public Button getBtnMultiplayer() {
        return btnMultiplayer;
    }

    public Button getBtnLeaderboard() {
        return btnLeaderboard;
    }

    public Button getBtnExit() {
        return btnExit;
    }
}
