package fr.univ_lyon1.groupen.poneymon.view;

import fr.univ_lyon1.groupen.poneymon.model.item.Item;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;

public class ItemView {
    private Image image;
    private GraphicsContext graphicsContext;
    private Item item;
    private static final boolean DEBUG = false;

    ItemView(GraphicsContext graphicsContext, Item item) {
        this.item = item;
        this.graphicsContext = graphicsContext;
        image = new Image("assets/Items/" + item.getName() + ".png");
    }

    /**
     * Affichage de l'item.
     * @param poney poney de la lane de l'item.
     */
    public void display(PoneyView poney) {
        if (item.isObservable(poney.getModel())) {
            final double width = item.getWidth();

            for (int i = 0; i < width; i++) {
                graphicsContext.drawImage(image,
                        item.getX() + i, item.getLane(),
                        1,1);
            }

            if (DEBUG) {
                graphicsContext.setLineWidth(0.02);
                graphicsContext.setStroke(Color.PURPLE);
                graphicsContext.strokeRect(item.getX(), item.getLane(), width, 1);
            }
        }

    }
}