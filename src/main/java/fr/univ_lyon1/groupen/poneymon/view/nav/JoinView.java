package fr.univ_lyon1.groupen.poneymon.view.nav;

import fr.univ_lyon1.groupen.poneymon.view.StateView;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.ImageView;

import java.io.IOException;

public class JoinView implements StateView {
    private Parent root;
    private ImageView btnBack;
    private ImageView btnRefresh;
    private Button btnValidate;
    private TableView tvCurrentGames;
    private TableColumn colNickname;
    private TableColumn colNrPlayers;
    private TableColumn colNrPlayersMax;
    private TableColumn colLength;
    private TableColumn colDifficulty;
    private TableColumn colRanked;

    public JoinView() throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/fxml/Join.fxml"));
        root = loader.load();
        btnBack = (ImageView) loader.getNamespace().get("button_back");
        btnRefresh = (ImageView) loader.getNamespace().get("button_refresh");
        btnValidate = (Button) loader.getNamespace().get("button_validate");
        tvCurrentGames = (TableView) loader.getNamespace().get("tableview_leaderboard");
        colNickname = (TableColumn) loader.getNamespace().get("column_pseudo");
        colNrPlayers = (TableColumn) loader.getNamespace().get("column_nbJoueurs");
        colNrPlayersMax = (TableColumn) loader.getNamespace().get("column_nbJoueursMax");
        colLength = (TableColumn) loader.getNamespace().get("column_longueur");
        colDifficulty = (TableColumn) loader.getNamespace().get("column_difficultee");
        colRanked = (TableColumn) loader.getNamespace().get("column_ranked");
    }

    public TableColumn getColNrPlayersMax() {
        return colNrPlayersMax;
    }

    public ImageView getBtnBack() {
        return btnBack;
    }

    public ImageView getBtnRefresh() {
        return btnRefresh;
    }

    public Button getBtnValidate() {
        return btnValidate;
    }

    public TableView getTvCurrentGames() {
        return tvCurrentGames;
    }

    public TableColumn getColNickname() {
        return colNickname;
    }

    public TableColumn getColNrPlayers() {
        return colNrPlayers;
    }

    public TableColumn getColLength() {
        return colLength;
    }

    public TableColumn getColDifficulty() {
        return colDifficulty;
    }

    public TableColumn getColRanked() {
        return colRanked;
    }

    @Override
    public Scene getScene() {
        return root.getScene();
    }

    @Override
    public Node getNode() {
        return root;
    }
}