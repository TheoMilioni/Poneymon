package fr.univ_lyon1.groupen.poneymon.view;

import javafx.scene.Node;
import javafx.scene.Scene;

public interface StateView {
    /**
     * Obtient la Scene JavaFX de la vue.
     * @return Instance de Scene JavaFX dont la vue fait partie.
     */
    Scene getScene();

    /**
     * Obtient le nœud JavaFX associé à la vue.
     * @return Instance d'un Node JavaFX.
     */
    Node getNode();
}
