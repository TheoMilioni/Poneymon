package fr.univ_lyon1.groupen.poneymon.view.nav;

import fr.univ_lyon1.groupen.poneymon.view.StateView;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;


import java.io.IOException;

public class CreateOrJoinView implements StateView {
    private Parent root;
    private Button btnCreate;
    private Button btnJoin;
    private ImageView btnBack;

    public CreateOrJoinView() throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/fxml/CreateOrJoin.fxml"));
        root = loader.load();
        btnBack = (ImageView) loader.getNamespace().get("button_back");
        btnCreate = (Button) loader.getNamespace().get("button_create");
        btnJoin = (Button) loader.getNamespace().get("button_join");

    }

    public Button getBtnCreate() {
        return btnCreate;
    }

    public Button getBtnJoin() {
        return btnJoin;
    }

    public ImageView getBtnBack() {
        return btnBack;
    }

    @Override
    public Scene getScene() {
        return root.getScene();
    }

    @Override
    public Node getNode() {
        return root;
    }
}