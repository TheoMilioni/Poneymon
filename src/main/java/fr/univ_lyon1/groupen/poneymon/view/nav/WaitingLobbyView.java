package fr.univ_lyon1.groupen.poneymon.view.nav;

import fr.univ_lyon1.groupen.poneymon.view.StateView;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;

import java.io.IOException;

public class WaitingLobbyView implements StateView {
    private Parent root;
    private ImageView btnBack;
    private Text txtNrPlayer;
    private Text txtNrMaxPlayer;


    public WaitingLobbyView() throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/fxml/WaitingLobby.fxml"));
        root = loader.load();
        btnBack = (ImageView) loader.getNamespace().get("button_back");
        txtNrPlayer = (Text) loader.getNamespace().get("text_nbrPlayers");
        txtNrMaxPlayer = (Text) loader.getNamespace().get("text_nbrJoueursMax");

    }

    public ImageView getBtnBack() {
        return btnBack;
    }

    public Text getTxtNrPlayer() {
        return txtNrPlayer;
    }

    public Text getTxtNrMaxPlayers() {
        return txtNrMaxPlayer;
    }

    @Override
    public Scene getScene() {
        return root.getScene();
    }

    @Override
    public Node getNode() {
        return root;
    }
}