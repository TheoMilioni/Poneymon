package fr.univ_lyon1.groupen.poneymon.view.nav;

import fr.univ_lyon1.groupen.poneymon.view.StateView;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;

import java.io.IOException;

public class MultiConnectionView implements StateView {

    private Parent root;
    private ImageView btnBack;
    private TextField pseudo;
    private PasswordField password;
    private Button btnSignup;
    private Button btnSignin;
    private Text txtInfos;

    public MultiConnectionView() throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/fxml/MultiConnection.fxml"));
        root = loader.load();
        btnBack = (ImageView) loader.getNamespace().get("button_back");
        pseudo = (TextField) loader.getNamespace().get("pseudo");
        password = (PasswordField) loader.getNamespace().get("password");
        btnSignup = (Button) loader.getNamespace().get("button_signup");
        btnSignin = (Button) loader.getNamespace().get("button_signin");
        txtInfos = (Text) loader.getNamespace().get("text_infos");
    }

    public Text getTxtInfos() {
        return txtInfos;
    }

    public ImageView getBtnBack() {
        return btnBack;
    }

    public String getPseudo() {
        return pseudo.getCharacters().toString();
    }

    public String getPassword() {
        return password.getCharacters().toString();
    }

    public TextField getPseudoField() {
        return pseudo;
    }

    public PasswordField getPasswordField() {
        return password;
    }

    public Button getBtnSignup() {
        return btnSignup;
    }

    public Button getBtnSignin() {
        return btnSignin;
    }

    @Override
    public Scene getScene() {
        return root.getScene();
    }

    @Override
    public Node getNode() {
        return root;
    }
}
