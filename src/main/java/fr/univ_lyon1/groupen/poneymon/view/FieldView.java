package fr.univ_lyon1.groupen.poneymon.view;

import fr.univ_lyon1.groupen.poneymon.controller.GameController;
import fr.univ_lyon1.groupen.poneymon.model.FieldModel;
import fr.univ_lyon1.groupen.poneymon.model.PoneyModel;
import fr.univ_lyon1.groupen.poneymon.model.item.Item;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.image.PixelReader;
import javafx.scene.image.WritableImage;
import javafx.scene.image.WritablePixelFormat;
import javafx.scene.paint.Color;

import java.util.logging.Logger;

/**
 * Classe gerant l'affichage du terrain de jeu.
 *
 */
public class FieldView extends Canvas {
    private static final Logger LOGGER = Logger.getLogger(FieldView.class.getName());

    /** Poneys. */
    private PoneyView[] poneys;
    /** Items. */
    private ItemView[] items;

    private final GraphicsContext gc;

    private FieldModel fieldModel;
    private int indexFocusedPoney;

    private Image[] backgrounds;

    /**
     * Canvas dans lequel on va dessiner le jeu.
     */
    public FieldView(int indexFocusedPoney) {
        super();
        this.indexFocusedPoney = indexFocusedPoney;
        

        /*
         * Permet de capturer le focus et donc les evenements clavier et
         * souris
         */
        this.setFocusTraversable(true);

        gc = getGraphicsContext2D();
    }

    private static int colorARGB(final Color color) {
        int r = (int)Math.round(color.getRed() * 255.0);
        int g = (int)Math.round(color.getGreen() * 255.0);
        int b = (int)Math.round(color.getBlue() * 255.0);
        int a = (int)Math.round(color.getOpacity() * 255.0);
        return (a << 24) | (r << 16) | (g << 8) | b;
    }

    /** Spécifie à la vue quel modèle utiliser.
     * Initialise le terrain de course.
     *
     * @param fieldModel Modèle à utiliser.
     */
    public void setModel(FieldModel fieldModel) {
        this.fieldModel = fieldModel;

        poneys = new PoneyView[fieldModel.getPoneyCount()];
        backgrounds = new Image[fieldModel.getPoneyCount()];
        final Image bgSource = new Image("assets/Items/track.png");
        final PixelReader bgSourcePixelReader = bgSource.getPixelReader();
        final int bgWidth = (int) bgSource.getWidth();
        final int bgHeight = (int) bgSource.getHeight();
        final int[] bgSourcePixels = new int[bgWidth * bgHeight];
        bgSourcePixelReader.getPixels(0, 0, bgWidth, bgHeight,
                WritablePixelFormat.getIntArgbInstance(),
                bgSourcePixels, 0, bgWidth);
        for (int i = 0; i < poneys.length; i++) {
            final PoneyModel model = fieldModel.getPoney(i);

            poneys[i] = new PoneyView(gc, model);
            long start = System.nanoTime();
            final WritableImage bg = new WritableImage(bgWidth, bgHeight);
            final int[] bgPixels = new int[bgWidth * bgHeight];
            final int primaryARGB = colorARGB(model.getColor().getPrimaryColor());
            final int secondaryARGB = colorARGB(model.getColor().getSecondaryColor());
            int px = 0;
            for (int y = 0; y < bgHeight; ++y) {
                for (int x = 0; x < bgWidth; ++x) {
                    if (bgSourcePixels[px] == 0xFFFF0000) {
                        bgPixels[px] = primaryARGB;
                    } else if (bgSourcePixels[px] == 0xFF00FF00) {
                        bgPixels[px] = secondaryARGB;
                    } else {
                        bgPixels[px] = bgSourcePixels[px];
                    }
                    ++px;
                }
            }
            bg.getPixelWriter().setPixels(0, 0, bgWidth, bgHeight,
                    WritablePixelFormat.getIntArgbInstance(),
                    bgPixels, 0, bgWidth);
            long end = System.nanoTime();
            LOGGER.fine(() -> (end - start) / 1000000.0 + " ms");
            backgrounds[i] = bg;
        }

        items = new ItemView[fieldModel.getItem().size()];

        for (int i = 0; i < items.length; i++) {
            Item model = fieldModel.getItem().get(i);
            items[i] = new ItemView(gc, model);
        }
    }

    FieldModel getModel() {
        return fieldModel;
    }

    /** Spécifie à la vue quel contrôleur y est attaché.
     *
     * @param controller GameController associé.
     */
    public void setController(GameController controller) {
        this.setOnKeyPressed(controller);
        this.setOnKeyReleased(controller);
    }

    /**
     * Affichage de la course.
     * Sert à nettoyer le canvas à chaque frame
     */
    public void display() {
        final double pxWidth = getWidth();
        final double pxHeight = getHeight();
        gc.setFill(Color.LIGHTGRAY);
        gc.fillRect(0, 0, pxWidth, pxHeight);

        gc.save();
        final double scale = pxHeight / (PoneyModel.LANE_COUNT * fieldModel.getPoneyCount());
        final double width = pxWidth / scale;
        gc.scale(scale, scale);

        // Affichage du background (fond)
        final double bgRatio = backgrounds[0].getWidth() / backgrounds[0].getHeight();
        final double bgWidth = PoneyModel.LANE_COUNT * bgRatio;
        int size = (int)(width / bgWidth + 2);
        double distance = poneys[indexFocusedPoney].getModel().getX();
        for (int y = 0; y < fieldModel.getPoneys().length; ++y) {
            for (int x = 0; x < size; x++) {
                gc.drawImage(backgrounds[y],
                        x * bgWidth - (distance % bgWidth),
                        y * PoneyModel.LANE_COUNT,
                        bgWidth,
                        PoneyModel.LANE_COUNT);
            }
        }

        gc.save();
        gc.translate(-poneys[indexFocusedPoney].getModel().getX() + PoneyView.PONEY_VIEW_OFFSET, 0);
        // Affichage des poneys
        for (PoneyView poney : poneys) {

            for (ItemView item : items) { // Affichage des items
                item.display(poney);
            }
            poney.display();
            gc.translate(0, PoneyModel.LANE_COUNT);
        }
        gc.restore();
        gc.restore();
    }


    /** Obtient les PoneyViews sur le terrain.
     *
     * @return Liste de PoneyView.
     */
    public PoneyView[] getPoneyViews() {
        return poneys;
    }

    @Override
    public double prefWidth(double height) {
        return getWidth();
    }

    @Override
    public double maxWidth(double height) {
        return Double.POSITIVE_INFINITY;
    }

    @Override
    public double prefHeight(double width) {
        return getHeight();
    }

    @Override
    public double maxHeight(double width) {
        return Double.POSITIVE_INFINITY;
    }

    @Override
    public boolean isResizable() {
        return true;
    }

    public double getTrackHeight() {
        return getHeight() / poneys.length;
    }

}
