package fr.univ_lyon1.groupen.poneymon.view.nav;

import fr.univ_lyon1.groupen.poneymon.model.Race;
import fr.univ_lyon1.groupen.poneymon.view.StateView;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.RadioButton;
import javafx.scene.image.ImageView;

import java.io.IOException;

public class MultiCreateView implements StateView {
    private Parent root;
    private ImageView btnBack;
    private ChoiceBox cbNrPlayers;
    private ChoiceBox cbFieldLength;
    private ChoiceBox cbDifficulty;
    private RadioButton rbRanked;
    private Button btnCreateGame;

    public MultiCreateView() throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/fxml/MultiCreate.fxml"));
        root = loader.load();
        btnBack = (ImageView) loader.getNamespace().get("button_back");
        cbNrPlayers = (ChoiceBox) loader.getNamespace().get("nbr_joueur");
        cbNrPlayers.getItems().addAll(2,3,4);
        cbNrPlayers.getSelectionModel().select(2);
        cbFieldLength = (ChoiceBox) loader.getNamespace().get("nbr_tour");
        cbFieldLength.getItems().addAll(100, 200, 300);
        cbFieldLength.getSelectionModel().select(0);
        cbDifficulty = (ChoiceBox) loader.getNamespace().get("difficulte");
        cbDifficulty.getItems().addAll(Race.Difficulty.EASY, Race.Difficulty.HARD);
        cbDifficulty.getSelectionModel().select(0);
        rbRanked = (RadioButton) loader.getNamespace().get("ranked");
        btnCreateGame = (Button) loader.getNamespace().get("button_createGame");

    }

    public ImageView getBtnBack() {
        return btnBack;
    }

    public ChoiceBox getCbNrPlayers() {
        return cbNrPlayers;
    }

    public ChoiceBox getCbFieldLength() {
        return cbFieldLength;
    }

    public ChoiceBox getCbDifficulty() {
        return cbDifficulty;
    }

    public RadioButton getRbRanked() {
        return rbRanked;
    }

    public Button getBtnCreateGame() {
        return btnCreateGame;
    }

    @Override
    public Scene getScene() {
        return root.getScene();
    }

    @Override
    public Node getNode() {
        return root;
    }
}
