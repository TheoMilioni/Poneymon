package fr.univ_lyon1.groupen.poneymon.view;

import fr.univ_lyon1.groupen.poneymon.model.PoneyModel;
import javafx.animation.Timeline;
import javafx.geometry.Rectangle2D;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.logging.Level;
import java.util.logging.Logger;

/** Affichage d'un poney.
 */
public class PoneyView {
    private static final Logger LOGGER = Logger.getLogger(PoneyView.class.getName());

    /**
     * Hauteur visuelle du poney, en mètres.
     */
    private static final double PONEY_HEIGHT = 2;
    /**
     * Nombre de mètres dont tous les poneys (incluant celui du joueur) doivent être décalés
     * vers la droite. Permet de voir le corps du poney focus.
     */
    static final double PONEY_VIEW_OFFSET = 3.5;
    /**
     * Boîte de collision du poney, en pixels sur l'image dessinée.
     */
    private static final Rectangle2D PONEY_HITBOX = new Rectangle2D(
            87, 10, 129, 102);

    /**
     * Flag indiquant si les boîtes de bordures doivent être affichées en plus de l'image
     * du poney.
     */
    private static final boolean DEBUG_DRAW = false;
    private static final boolean DEBUG_MODEL = false;

    private final PoneyModel poneyModel;

    // On cree trois images globales pour ne pas les recreer en permanence
    private Image currentPoney;
    private final Image poneyImage;
    private final Image poneyImageBoost;
    private double imageWidthInM;

    private GraphicsContext graphicsContext;

    private Field animationField;
    private Method stop;
    private Method start;
    private Field timelineField;

    /**
     * Constructeur du PoneyView.
     * @param gc ContextGraphic dans lequel on va afficher le poney.
     * @param poneyModel Modèle a associer à la vue.
     */
    PoneyView(GraphicsContext gc, PoneyModel poneyModel) {
        this.poneyModel = poneyModel;

        if (gc != null) {
            // gc == null can be provided for testing
            graphicsContext = gc;

            // On charge l'image associée au poney
            poneyImage = new Image(poneyModel.getColor().getRunningImagePath());
            poneyImageBoost = new Image(poneyModel.getColor().getRainbowImagePath());

            currentPoney = poneyImage;
            try {
                animationField = currentPoney.getClass().getDeclaredField("animation");
                animationField.setAccessible(true);
                stop = animationField.getType().getDeclaredMethod("stop");
                stop.setAccessible(true);
                start = animationField.getType().getDeclaredMethod("start");
                start.setAccessible(true);
                timelineField = animationField.getType().getDeclaredField("timeline");
                timelineField.setAccessible(true);
            } catch (NoSuchFieldException | NoSuchMethodException e) {
                LOGGER.log(Level.SEVERE, e.getMessage(), e);
            }

            imageWidthInM = PONEY_HEIGHT * (currentPoney.getWidth() / currentPoney.getHeight());

            poneyModel.onBoostChange.subscribe((sourceObject, eventData) -> {
                if (poneyModel.isBoost()) {
                    currentPoney = poneyImageBoost;
                } else {
                    currentPoney = poneyImage;
                }
            });
        } else {
            poneyImage = null;
            poneyImageBoost = null;
        }

    }

    /**
     *  Affichage du poney.
     */
    public void display() {
        final double width = imageWidthInM;

        final double height = PONEY_HEIGHT;
        final double x = poneyModel.getX()
                - width * (PONEY_HITBOX.getMaxX() / currentPoney.getWidth());

        double yPoney = poneyModel.getLane() - 1.;
        graphicsContext.drawImage(currentPoney,
                x, yPoney,
                width, height);

        //Animation du poney
        if (poneyModel.isFreeeze()) {
            stopAnimation();
        } else {
            startAnimation();
        }
        getAnimationSpeed(poneyModel.getSpeed() / 4);

        if (DEBUG_DRAW) {
            graphicsContext.setLineWidth(0.02);
            graphicsContext.setStroke(Color.RED);
            graphicsContext.strokeRect(x, 0, width, height);
            graphicsContext.setStroke(Color.GREEN);
            graphicsContext.strokeRect(
                    x + width * (PONEY_HITBOX.getMinX() / currentPoney.getWidth()),
                    height * (PONEY_HITBOX.getMinY() / currentPoney.getHeight()),
                    width * (PONEY_HITBOX.getWidth() / currentPoney.getWidth()),
                    height * (PONEY_HITBOX.getHeight() / currentPoney.getHeight()));
        }

        if (DEBUG_MODEL) {
            graphicsContext.setStroke(Color.BLACK);
            graphicsContext.strokeRect(
                    poneyModel.getX() - 1.5,
                    yPoney + 1,
                    0.5,
                    1);
        }

    }

    public PoneyModel getModel() {
        return poneyModel;
    }

    private double getAnimationSpeed() {
        try {
            Timeline timeline = (Timeline) timelineField.get(animationField.get(currentPoney));
            return timeline.getRate();
        } catch (IllegalAccessException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }
        return 1;
    }

    private void getAnimationSpeed(double speed) {
        try {
            Timeline timeline = (Timeline) timelineField.get(animationField.get(currentPoney));
            timeline.setRate(speed);
        } catch (IllegalAccessException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }
    }

    private void startAnimation() {
        try {
            start.invoke(animationField.get(currentPoney));
        } catch (IllegalAccessException | InvocationTargetException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }
    }

    private void stopAnimation() {
        try {
            stop.invoke(animationField.get(currentPoney));
        } catch (IllegalAccessException | InvocationTargetException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }
    }
}
