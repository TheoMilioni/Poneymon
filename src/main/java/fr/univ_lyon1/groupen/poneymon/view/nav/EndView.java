package fr.univ_lyon1.groupen.poneymon.view.nav;

import fr.univ_lyon1.groupen.poneymon.view.StateView;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.text.Text;

import java.io.IOException;

public class EndView implements StateView {
    private Parent root;
    private Button btnMainMenu;
    private Text txtWinner;

    public EndView() throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/fxml/End.fxml"));
        root = loader.load();
        btnMainMenu = (Button) loader.getNamespace().get("button_mainMenu");
        txtWinner = (Text) loader.getNamespace().get("text_winner");
    }

    public Button getBtnMainMenu() {
        return btnMainMenu;
    }

    public Text getTxtWinner() {
        return txtWinner;
    }

    @Override
    public Scene getScene() {
        return root.getScene();
    }

    @Override
    public Node getNode() {
        return root;
    }
}