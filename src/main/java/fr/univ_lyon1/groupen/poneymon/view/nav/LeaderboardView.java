package fr.univ_lyon1.groupen.poneymon.view.nav;

import fr.univ_lyon1.groupen.poneymon.view.StateView;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.ImageView;

import java.io.IOException;

public class LeaderboardView implements StateView {
    private Parent root;
    private ImageView btnBack;
    private TableView tvLeaderboard;
    private TableColumn tcNickname;
    private TableColumn tcScore;


    public LeaderboardView() throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/fxml/LeaderBoard.fxml"));
        root = loader.load();
        btnBack = (ImageView) loader.getNamespace().get("button_back");
        tvLeaderboard = (TableView) loader.getNamespace().get("tableview_leaderboard");
        tcNickname = (TableColumn) loader.getNamespace().get("tablecolumn_pseudo");
        tcScore = (TableColumn) loader.getNamespace().get("tablecolumn_score");
    }

    public TableView getTvLeaderboard() {
        return tvLeaderboard;
    }

    public TableColumn getTcNickname() {
        return tcNickname;
    }

    public TableColumn getTcScore() {
        return tcScore;
    }

    public ImageView getBtnBack() {
        return btnBack;
    }

    @Override
    public Scene getScene() {
        return root.getScene();
    }

    @Override
    public Node getNode() {
        return root;
    }
}
