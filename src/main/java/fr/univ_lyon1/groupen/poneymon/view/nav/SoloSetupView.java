package fr.univ_lyon1.groupen.poneymon.view.nav;

import fr.univ_lyon1.groupen.poneymon.model.Race;
import fr.univ_lyon1.groupen.poneymon.view.StateView;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;

import java.io.IOException;

public class SoloSetupView implements StateView {
    private ImageView btnBack;
    private Parent root;
    private ChoiceBox cbFieldLength;
    private ChoiceBox cbDifficulty;
    private Button buttonValidate;
    private GridPane gridpaneListeProtoPoneys;

    public SoloSetupView() throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/fxml/SoloSetup.fxml"));
        root = loader.load();
        buttonValidate = (Button)loader.getNamespace().get("button_validate");
        btnBack = (ImageView) loader.getNamespace().get("button_back");
        cbFieldLength = (ChoiceBox) loader.getNamespace().get("length");
        cbFieldLength.getItems().addAll(100, 200, 300);
        cbFieldLength.getSelectionModel().select(0);
        cbDifficulty = (ChoiceBox) loader.getNamespace().get("difficulty");
        cbDifficulty.getItems().addAll(Race.Difficulty.EASY, Race.Difficulty.HARD);
        cbDifficulty.getSelectionModel().select(0);
        gridpaneListeProtoPoneys = (GridPane) loader.getNamespace().get(
                "gridpane_listeProtoPoneys");
    }

    public Button getButtonValidate() {
        return buttonValidate;
    }

    public ImageView getBtnBack() {
        return btnBack;
    }

    public GridPane getGridpaneListeProtoPoneys() {
        return gridpaneListeProtoPoneys;
    }

    public Node getProtoFromGridPane(int col) {
        for (Node node : gridpaneListeProtoPoneys.getChildren()) {
            if (GridPane.getColumnIndex(node) == col) {
                return node;
            }
        }
        return null;
    }

    public ChoiceBox getCbFieldLength() {
        return cbFieldLength;
    }

    public ChoiceBox getCbDifficulty() {
        return cbDifficulty;
    }

    @Override
    public Scene getScene() {
        return root.getScene();
    }

    @Override
    public Node getNode() {
        return root;
    }
}
