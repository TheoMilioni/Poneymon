package fr.univ_lyon1.groupen.poneymon.view;

import fr.univ_lyon1.groupen.poneymon.controller.GameController;
import fr.univ_lyon1.groupen.poneymon.model.FieldModel;
import fr.univ_lyon1.groupen.poneymon.model.PoneyModel;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

import java.text.DecimalFormat;

/**
 * Classe gerant le terrain de jeu.
 *
 */
public class SidePoneyView extends Canvas {

    /** Poneys. */
    private FieldModel fieldModel;

    private final GraphicsContext gc;

    private GameController controller;

    /**
     * Canvas dans lequel on va dessiner le jeu.
     */
    public SidePoneyView() {
        super();

        /*
         * Permet de capturer le focus et donc les evenements clavier et
         * souris
         */
        this.setFocusTraversable(true);

        gc = getGraphicsContext2D();
    }

    /**
    * Le poney à afficher les informations.
    * @param fieldModel le modèle du terrain
    */
    public void setModel(FieldModel fieldModel) {
        this.fieldModel = fieldModel;
    }

    /**
    * Fournir à la vue le controller.
    * @param controller le contrôlleur
    */
    public void setController(GameController controller) {
        this.controller = controller;
        this.setOnKeyPressed(controller);
        this.setOnKeyReleased(controller);
    }

    /**
    * Affichage du champ de course.
    * Sert à nettoyer le canvas à chaque frame
    */
    public void display() {
        gc.setFill(Color.LIGHTGRAY);
        gc.fillRect(0, 0, getWidth(), getHeight());

        gc.setFill(Color.BLACK);
        PoneyModel focusedPoney = this.controller.getFocusedPoney();
        gc.fillText("Couleur : " + focusedPoney.getColor().getName(), 20, 20);
        DecimalFormat decFmt = new DecimalFormat("#.##");
        gc.fillText("Position X : " + decFmt.format(focusedPoney.getX()) + " m", 20, 40);
        gc.fillText("Vitesse : " + decFmt.format(focusedPoney.getSpeed()), 20, 60);
        gc.fillText("Accélération : " + decFmt.format(focusedPoney.getAcceleration()), 20, 80);
        gc.fillText("Vistesse maximale autorisée : "
                + decFmt.format(focusedPoney.getMaxSpeed()), 20, 100);
        gc.fillText("Lane : " + decFmt.format(focusedPoney.getLane()), 20, 120);

        gc.fillText("Etats : ", 20, 200);
        gc.fillText("Freeze : " + focusedPoney.isFreeeze(), 30, 220);
        gc.fillText("Nerf : " + focusedPoney.isNerf(), 30, 240);
        gc.fillText("Boost : " + focusedPoney.isBoost(), 30, 260);
        gc.fillText("CanBoost : " + focusedPoney.isBoostAvailable(), 30, 280);

        miniMapDisplay();
    }

    /**
     * Affichage de la minimap.
     */
    public void miniMapDisplay() {
        gc.setStroke(Color.BLACK);
        gc.setLineWidth(2);
        gc.strokeLine(10, 180, 190, 180);
        for (PoneyModel poney : fieldModel.getPoneys()) {
            gc.setFill(poney.getColor().getPrimaryColor());
            gc.fillOval(convertorX(poney.getX()), 175, 10, 10);
        }
    }
    
    public double convertorX(double x) {
        double totalRace = fieldModel.getCurrentRace().getRaceLength();
        return 10 + (170 * x) / totalRace;
    }

    @Override
    public double prefWidth(double height) {
        return getWidth();
    }

    @Override
    public double prefHeight(double width) {
        return getHeight();
    }

    @Override
    public boolean isResizable() {
        return true;
    }

}
