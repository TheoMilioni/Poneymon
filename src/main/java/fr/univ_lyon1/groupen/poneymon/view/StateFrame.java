package fr.univ_lyon1.groupen.poneymon.view;

import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class StateFrame {
    private static final int DEFAULT_WIDTH = 1000;
    private static final int DEFAULT_HEIGHT = 800;

    private final AnchorPane mPane = new AnchorPane();
    private final Scene mScene;

    public StateFrame(Stage stage) {
        this(stage, DEFAULT_WIDTH, DEFAULT_HEIGHT);
    }

    /**
     * Créé le cadre principal d'affichage.
     * @param stage Stage JavaFX parente.
     * @param width Largeur de la fenêtre.
     * @param height Hauteur de la fenêtre.
     */
    public StateFrame(Stage stage, int width, int height) {
        mScene = new Scene(mPane, width, height);
        mPane.setBackground(new Background(new BackgroundFill(Color.WHITE, null, null)));
        stage.setScene(mScene);
    }

    /**
     * Remplace la vue contenue dans le cadre principal.
     * @param view Vue a utiliser.
     */
    public void setView(StateView view) {
        mPane.getChildren().clear();
        Node node = view.getNode();
        AnchorPane.setTopAnchor(node, 0.0);
        AnchorPane.setLeftAnchor(node, 0.0);
        AnchorPane.setRightAnchor(node, 0.0);
        AnchorPane.setBottomAnchor(node, 0.0);
        mPane.getChildren().add(node);
    }
}
