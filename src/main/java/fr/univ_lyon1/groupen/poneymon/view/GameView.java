package fr.univ_lyon1.groupen.poneymon.view;

import fr.univ_lyon1.groupen.poneymon.controller.GameController;
import fr.univ_lyon1.groupen.poneymon.model.FieldModel;
import javafx.scene.Node;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;


public class GameView implements StateView {
    private static final boolean SIDE_WINDOW = false;

    private final BorderPane root;
    private FieldView fieldView;
    private SidePoneyView sideView;
    private final MinimapView minimapView;
    private final int indexFocusedPoney;

    /**
     * Constructeur.
     */
    public GameView() {
        indexFocusedPoney = 0;
        this.fieldView = new FieldView(indexFocusedPoney);

        root = new BorderPane();

        Pane minimapPane = new Pane();
        minimapView = new MinimapView();
        minimapPane.getChildren().add(minimapView);
        minimapView.widthProperty().bind(
                minimapPane.widthProperty());
        minimapView.heightProperty().bind(
                minimapPane.heightProperty());
        root.setTop(minimapPane);

        // On ajoute le terrain de jeu au centre de la fenêtre.
        // Avec de quoi le redimensionner, tant qu'on y est.
        Pane fieldPane = new Pane();
        fieldPane.getChildren().add(fieldView);
        fieldView.widthProperty().bind(
                fieldPane.widthProperty());
        fieldView.heightProperty().bind(
                fieldPane.heightProperty());
        root.setCenter(fieldPane);

        if (SIDE_WINDOW) {
            Stage sideStage = new Stage();
            sideStage.setTitle("Information Poney");
            this.sideView = new SidePoneyView();
            final BorderPane root2 = new BorderPane();
            final Scene scene2 = new Scene(root2, 250, 300);
            Pane pane = new Pane();
            pane.getChildren().add(sideView);
            sideView.widthProperty().bind(pane.widthProperty());
            sideView.heightProperty().bind(pane.heightProperty());
            root2.setCenter(pane);
            sideStage.setScene(scene2);
            sideStage.show();
        }
    }
    
    /**
    * Ajoute un modèle à la vue.
    * @param fieldModel le modèle de terrain
    */
    public void setModel(FieldModel fieldModel) {
        this.fieldView.setModel(fieldModel);
        this.minimapView.setModel(fieldModel);
        if (SIDE_WINDOW) {
            this.sideView.setModel(fieldModel);
        }
    }
        
    /**
    * Ajoute un contorlleur à vue et l'associe aux sous-vues.
    * @param controller le controller
    */
    public void setController(GameController controller) {
        controller.setFocusedPoney(indexFocusedPoney);
        this.fieldView.setController(controller);
        if (SIDE_WINDOW) {
            this.sideView.setController(controller);
        }
    }

    /**
     * Rafraîchit l'affichage du jeu.
     */
    public void display() {
        this.fieldView.display();
        this.minimapView.display();
        if (SIDE_WINDOW) {
            this.sideView.display();
        }
    }

    @Override
    public Scene getScene() {
        return root.getScene();
    }

    @Override
    public Node getNode() {
        return root;
    }
}
