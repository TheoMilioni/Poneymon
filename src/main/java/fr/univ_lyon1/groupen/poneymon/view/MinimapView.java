package fr.univ_lyon1.groupen.poneymon.view;

import fr.univ_lyon1.groupen.poneymon.model.FieldModel;
import fr.univ_lyon1.groupen.poneymon.model.PoneyModel;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class MinimapView extends Canvas {
    private static final int HORIZONTAL_MARGIN = 10;

    private final GraphicsContext gc;

    private FieldModel fieldModel;

    /**
     * Constucteur de la minimap.
     */
    public MinimapView() {
        super();
        this.setFocusTraversable(false);
        gc = getGraphicsContext2D();
    }

    public void setModel(FieldModel fieldModel) {
        this.fieldModel = fieldModel;
    }

    @Override
    public double prefWidth(double height) {
        return getWidth();
    }

    @Override
    public double maxWidth(double height) {
        return Double.MAX_VALUE;
    }

    @Override
    public double minHeight(double width) {
        return 32.;
    }

    @Override
    public boolean isResizable() {
        return true;
    }

    /**
     * Affichage de la minimap.
     */
    public void display() {
        gc.setFill(Color.LIGHTGRAY);
        gc.fillRect(0, 0, getWidth(), getHeight());
        gc.setStroke(Color.BLACK);
        gc.setLineWidth(2);
        final double lineWidth = getWidth() - HORIZONTAL_MARGIN;
        gc.strokeLine(HORIZONTAL_MARGIN, 16, lineWidth, 16);
        final double raceLength = fieldModel.getCurrentRace().getRaceLength();
        for (PoneyModel poney : fieldModel.getPoneys()) {
            gc.setFill(poney.getColor().getPrimaryColor());
            gc.fillOval(HORIZONTAL_MARGIN + lineWidth / raceLength * poney.getX(), 11, 10, 10);
        }
    }
}
