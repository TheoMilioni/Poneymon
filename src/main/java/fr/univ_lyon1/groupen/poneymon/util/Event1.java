package fr.univ_lyon1.groupen.poneymon.util;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

/**
 * Generic class representing an(y kind of) event, allowing subscription and
 * unsubscription of a callback.
 * @param <O> Type of the event source object.
 * @param <T1> Type of the event data to be passed to callbacks.
 */
@SuppressWarnings("unused")
public class Event1<O, T1> {
    public interface Callback<O, T1> {
        void onEventFired(final O sourceObject, final T1 eventData);
    }

    private final WeakReference<O> sourceObject;
    private final List<Callback<O, T1>> callbacks = new ArrayList<>();

    /**
     * Constructs an event.
     * @param sourceObject Object from which the events fired from fire()
     *                     comes from.
     */
    public Event1(final O sourceObject) {
        this.sourceObject = new WeakReference<>(sourceObject);
    }

    /**
     * Request a callback to be called when the event is fired.
     * @param callback Callback to subscribe to event firing.
     */
    public void subscribe(Callback<O, T1> callback) {
        callbacks.add(callback);
    }

    /**
     * Remove a callback from the event's callback list.
     * @param callback Callback to unsubscribe from event firing.
     */
    public void unsuscribe(Callback<O, T1> callback) {
        callbacks.remove(callback);
    }

    /**
     * Fire the event. Calls the callbacks appropriately.
     * @param eventData Event data to pass to callbacks.
     */
    public void fire(final T1 eventData) {
        for (Callback<O, T1> callback : callbacks) {
            callback.onEventFired(sourceObject.get(), eventData);
        }
    }
}
