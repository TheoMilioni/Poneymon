package fr.univ_lyon1.groupen.poneymon.util;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

/**
 * Generic class representing an(y kind of) event, allowing subscription and
 * unsubscription of a callback.
 * @param <O> Type of the event source object.
 * @param <T1> Type of the 1st event data to be passed to callbacks.
 * @param <T2> Type of the 2nd event data to be passed to callbacks.
 */
@SuppressWarnings("unused")
public class Event2<O, T1, T2> {
    public interface Callback<O, T1, T2> {
        void onEventFired(final O sourceObject, final T1 eventData1, final T2 eventData2);
    }

    private final WeakReference<O> sourceObject;
    private final List<Callback<O, T1, T2>> callbacks = new ArrayList<>();

    /**
     * Constructs an event.
     * @param sourceObject Object from which the events fired from fire()
     *                     comes from.
     */
    public Event2(final O sourceObject) {
        this.sourceObject = new WeakReference<>(sourceObject);
    }

    /**
     * Request a callback to be called when the event is fired.
     * @param callback Callback to subscribe to event firing.
     */
    public void subscribe(Callback<O, T1, T2> callback) {
        callbacks.add(callback);
    }

    /**
     * Remove a callback from the event's callback list.
     * @param callback Callback to unsubscribe from event firing.
     */
    public void unsuscribe(Callback<O, T1, T2> callback) {
        callbacks.remove(callback);
    }

    /**
     * Fire the event. Calls the callbacks appropriately.
     * @param eventData1 1st event data to pass to callbacks.
     * @param eventData2 2nd event data to pass to callbacks.
     */
    public void fire(final T1 eventData1, final T2 eventData2) {
        for (Callback<O, T1, T2> callback : callbacks) {
            callback.onEventFired(sourceObject.get(), eventData1, eventData2);
        }
    }
}
