package fr.univ_lyon1.groupen.poneymon.util;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

/**
 * Generic class representing an(y kind of) event, allowing subscription and
 * unsubscription of a callback.
 * @param <O> Type of the event source object.
 */
@SuppressWarnings("unused")
public class Event<O> {
    public interface Callback<O> {
        void onEventFired(final O sourceObject);
    }

    private final WeakReference<O> sourceObject;
    private final List<Callback<O>> callbacks = new ArrayList<>();

    /**
     * Constructs an event.
     * @param sourceObject Object from which the events fired from fire()
     *                     comes from.
     */
    public Event(final O sourceObject) {
        this.sourceObject = new WeakReference<>(sourceObject);
    }

    /**
     * Request a callback to be called when the event is fired.
     * @param callback Callback to subscribe to event firing.
     */
    public void subscribe(Callback<O> callback) {
        callbacks.add(callback);
    }

    /**
     * Remove a callback from the event's callback list.
     * @param callback Callback to unsubscribe from event firing.
     */
    public void unsuscribe(Callback<O> callback) {
        callbacks.remove(callback);
    }

    /**
     * Fire the event. Calls the callbacks appropriately.
     */
    public void fire() {
        for (Callback<O> callback : callbacks) {
            callback.onEventFired(sourceObject.get());
        }
    }
}
