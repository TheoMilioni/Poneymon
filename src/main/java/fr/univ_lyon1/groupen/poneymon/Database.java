package fr.univ_lyon1.groupen.poneymon;

import fr.univ_lyon1.groupen.poneymon.util.ServerSide;
import org.mindrot.jbcrypt.BCrypt;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Logger;

@ServerSide
public abstract class Database {
    private static final Logger LOGGER = Logger.getLogger(Database.class.getName());

    // Define the BCrypt workload to use when generating password hashes. 10-31 is a valid value.
    private static int workload = 12;

    private Database() {}

    /**
     * This method can be used to generate a string representing an account password
     * suitable for storing in a database. It will be an OpenBSD-style crypt(3) formatted
     * hash string of length=60
     * The bcrypt workload is specified in the above static variable, a value from 10 to 31.
     * A workload of 12 is a very reasonable safe default as of 2013.
     * This automatically handles secure 128-bit salt generation and storage within the hash.
     * @param passwordPlaintext
     * The account's plaintext password as provided during account creation,
     *			     or when changing an account's password.
     * @return String - a string of length 60 that is the bcrypt hashed password in crypt(3) format.
     */
    public static String hashPassword(String passwordPlaintext) {
        String salt = BCrypt.gensalt(workload);
        return BCrypt.hashpw(passwordPlaintext, salt);
    }

    /**
     * This method can be used to verify a computed hash from a plaintext (e.g. during a login
     * request) with that of a stored hash from a database. The password hash from the database
     * must be passed as the second variable.
     * @param passwordPlaintext The account's plaintext password, as provided during a
     *                          login request
     * @param storedHash The account's stored password hash, retrieved from the
     *                   authorization database
     * @return boolean - true if the password matches the password of the stored hash,
     *         false otherwise
     */
    public static boolean checkPassword(String passwordPlaintext, String storedHash) {
        if (null == storedHash || !storedHash.startsWith("$2a$")) {
            throw new java.lang.IllegalArgumentException("Invalid hash provided for comparison");
        }
        return BCrypt.checkpw(passwordPlaintext, storedHash);
    }

    /**
     * Connexion.
     */
    static boolean connexion(Connection conn, String idSaisie, String mdpSaisie)
            throws SQLException {
        // Requête de vérification
        try (PreparedStatement verif = conn.prepareStatement(
                "SELECT identifiant, mot_de_passe FROM Joueur WHERE identifiant = ?")) {
            verif.setString(1, idSaisie);
            try (ResultSet rs = verif.executeQuery()) {
                // Algo
                if (rs.next()) {
                    if (checkPassword(mdpSaisie, rs.getString(2))) {
                        LOGGER.fine("Mot de passe valide");
                        return true;
                    } else {
                        LOGGER.fine("Mot de passe incorrect");
                    }
                } else {
                    LOGGER.fine("Cet identifiant n'existe pas");
                }
            }
        }
        return false;
    }

    static boolean enregistrement(Connection conn, String idSaisie, String mdpSaisie)
            throws SQLException {
        // Requête de vérification
        try (PreparedStatement verif = conn.prepareStatement(
                "SELECT identifiant FROM Joueur WHERE identifiant = ?")) {
            verif.setString(1, idSaisie);
            try (ResultSet rs = verif.executeQuery()) {
                // Algo
                if (rs.next()) {
                    LOGGER.fine("L'identifiant choisit n'est pas disponible");
                } else {
                    // Requête d'insertion d'un nouvel utilisateur
                    try (PreparedStatement ajout = conn.prepareStatement(
                            "INSERT INTO Joueur(IDENTIFIANT,MOT_DE_PASSE," +
                                    "MEILLEUR_SCORE) VALUES (?,?,?)")) {
                        ajout.setString(1, idSaisie);
                        ajout.setString(2, hashPassword(mdpSaisie));
                        ajout.setInt(3, 0);
                        ajout.executeUpdate();
                    }
                    LOGGER.fine("Enregistrement pris en compte");
                    return true;
                }
            }
        }
        return false;
    }

    private static boolean mettreAJourScore(Connection conn, String id, int meilleurScore)
            throws SQLException {
        // Requête de vérification
        try (PreparedStatement verif = conn.prepareStatement(
                "SELECT identifiant, meilleur_score FROM Joueur WHERE identifiant = ?")) {
            verif.setString(1, id);
            try (ResultSet rs = verif.executeQuery()) {
                // Algo
                if (rs.next()) {
                    if (rs.getInt(2) < meilleurScore) {
                        LOGGER.fine("L'utilisateur a battu son meilleur score");
                        // Requête de mise à jour du score
                        try (PreparedStatement modif = conn.prepareStatement(
                                "UPDATE Joueur SET meilleur_score = ? WHERE identifiant = ?")) {
                            modif.setInt(1, meilleurScore);
                            modif.setString(2, id);
                            modif.executeUpdate();
                        }
                    } else {
                        LOGGER.fine("L'utilisateur n'a pas battu son reccord");
                    }
                    return true;
                } else {
                    LOGGER.fine("L'utilisateur n'est pas enregistrer dans la base.");
                }
            }
        }
        return false;
    }
}