package fr.univ_lyon1.groupen.poneymon;

import fr.univ_lyon1.groupen.poneymon.model.LeaderboardModel;
import fr.univ_lyon1.groupen.poneymon.model.LobbyModel;
import fr.univ_lyon1.groupen.poneymon.model.PlayerModel;
import fr.univ_lyon1.groupen.poneymon.model.ProtoPoney;
import fr.univ_lyon1.groupen.poneymon.net.Network;
import fr.univ_lyon1.groupen.poneymon.net.ServerBase;
import fr.univ_lyon1.groupen.poneymon.net.msg.DebugMessage;
import fr.univ_lyon1.groupen.poneymon.net.msg.LeaderboardResponse;
import fr.univ_lyon1.groupen.poneymon.net.msg.LobbyJoinRequest;
import fr.univ_lyon1.groupen.poneymon.net.msg.LobbyJoinResponse;
import fr.univ_lyon1.groupen.poneymon.net.msg.LobbyListResponse;
import fr.univ_lyon1.groupen.poneymon.net.msg.LobbyPlayerJoinMessage;
import fr.univ_lyon1.groupen.poneymon.net.msg.LobbyPlayerLeaveMessage;
import fr.univ_lyon1.groupen.poneymon.net.msg.LoginRequest;
import fr.univ_lyon1.groupen.poneymon.net.msg.LoginResponse;
import fr.univ_lyon1.groupen.poneymon.net.msg.Message;
import fr.univ_lyon1.groupen.poneymon.net.msg.MessageType;
import fr.univ_lyon1.groupen.poneymon.net.msg.PoneySelectBeginMessage;
import fr.univ_lyon1.groupen.poneymon.net.msg.PoneySelectUpdateMessage;
import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Server extends ServerBase {
    private static final Logger LOGGER = Logger.getLogger(Server.class.getName());

    private Random random = new Random();
    private Map<Long, LobbyModel> lobbies = new HashMap<>();

    private Connection db;

    /**
     * Point d'entrée du serveur.
     */
    public static void main(String[] args) throws IOException, InterruptedException,
            ClassNotFoundException, SQLException {
        int port;
        try {
            port = Integer.parseInt(args[0]);
        } catch (Exception ignored) {
            port = Network.DEFAULT_PORT;
        }
        ServerBase s = new Server(port);
        s.start();

        BufferedReader sysin = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            String in = sysin.readLine();
            s.broadcast(in);
            if (in.equals("exit")) {
                s.stop(1000);
                break;
            }
        }
    }

    private Server(int port) throws ClassNotFoundException, SQLException {
        super(port);
        Class.forName("org.postgresql.Driver");
        LOGGER.info("Driver O.K.");

        String url = "jdbc:postgresql://localhost:5432/poneymon";
        String user = "poneymon";
        String passwd = "poneymo" + Character.toLowerCase('n');

        db = DriverManager.getConnection(url, user, passwd);
        LOGGER.info("Connexion effective !");

    }

    @Override
    public void onStart() {
        LOGGER.info(() -> "Poneymon server, port " + getPort());
    }

    @Override
    public void onOpen(WebSocket conn, ClientHandshake handshake) {
        LOGGER.info(() -> "Hello " + sockRemote(conn));
        PlayerModel plrModel = new PlayerModel();
        plrModel.setWebSocket(conn);
        conn.setAttachment(plrModel);
    }

    @Override
    protected void onMessage(WebSocket conn, Message msg) throws SQLException {
        MessageType msgType = msg.getType();
        switch (msgType) {
            case DEBUG:
                DebugMessage dmsg = (DebugMessage) msg;
                LOGGER.info(() -> "Debug message from {}: {}" + sockRemote(conn)
                        + ": " + dmsg.getDebugText());
                break;
            case LOGIN_REQUEST:
                handleLoginRequest(conn, (LoginRequest) msg);
                break;
            case LOBBY_LIST_REQUEST:
                LobbyModel.Description[] descriptions = new LobbyModel.Description[lobbies.size()];
                int i = 0;
                for (final LobbyModel lobby : lobbies.values()) {
                    descriptions[i] = lobby.getDescription();
                    ++i;
                }
                LobbyListResponse resp = new LobbyListResponse();
                resp.setLobbiesDescriptions(descriptions);
                send(conn, resp);
                break;
            case LOBBY_JOIN_REQUEST:
                handleLobbyJoinRequest(conn, conn.getAttachment(), (LobbyJoinRequest) msg);
                break;
            case PONEY_SELECT_UPDATE:
                handlePoneySelectUpdate(conn.getAttachment(), (PoneySelectUpdateMessage) msg);
                break;
            case LOBBY_LEAVE:
                handleLobbyLeave(conn.getAttachment());
                break;
            case LEADERBOARD_REQUEST:
                handleLeaderboardRequest(conn);
                break;
            default:
                break;
        }
    }

    private void handleLoginRequest(WebSocket conn, LoginRequest msg) {
        LoginResponse resp = new LoginResponse();
        try {
            if (msg.getCreateIfNonexistent()) {
                if (Database.enregistrement(db, msg.getUsername(), msg.getPassword())) {
                    resp.setSuccess(true);
                    ((PlayerModel)conn.getAttachment()).setNickname(msg.getUsername());
                    ((PlayerModel)conn.getAttachment()).setPlayerId(msg.getUsername().hashCode());
                    resp.setPlayerId(msg.getUsername().hashCode());
                } else {
                    resp.setSuccess(false);
                    resp.setMessage("Nom d'utilisateur déjà pris");  // I18N!
                }
            } else {
                if (Database.connexion(db, msg.getUsername(), msg.getPassword())) {
                    resp.setSuccess(true);
                    ((PlayerModel)conn.getAttachment()).setNickname(msg.getUsername());
                    ((PlayerModel)conn.getAttachment()).setPlayerId(msg.getUsername().hashCode());
                    resp.setPlayerId(msg.getUsername().hashCode());
                } else {
                    resp.setSuccess(false);
                    resp.setMessage("Utilisateur/mot de passe invalide");  // I18N!
                }
            }
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
            resp.setSuccess(false);
            resp.setMessage(e.getMessage());
        }
        send(conn, resp);
    }

    /**
     * Gère les messages de création ou <i>join</i> de lobby.
     */
    private void handleLobbyJoinRequest(WebSocket conn, PlayerModel player, LobbyJoinRequest msg) {
        LobbyJoinResponse resp = new LobbyJoinResponse();
        if (msg.getLobbyId() == 0) {
            LobbyModel lobbyTemplate = msg.getLobbyTemplate();
            // Création d'un lobby
            LobbyModel lobby = new LobbyModel(
                    random.nextLong(),
                    player.getNickname(),
                    lobbyTemplate.getDescription().getRace());
            lobby.getDescription().setPlayerLimit(lobbyTemplate.getDescription().getPlayerLimit());
            lobby.addPlayer(player);
            lobbies.put(lobby.getDescription().getLobbyId(), lobby);
            resp.setSuccess(true);
            resp.setLobby(lobby);
            LOGGER.info(() -> player + " created lobby " + lobby.getDescription().getLobbyId());
            send(conn, resp);
        } else {
            LobbyModel lobby = lobbies.get(msg.getLobbyId());
            if (lobby == null) {
                resp.setSuccess(false);
                resp.setMessage("Lobby doesn't exist");
                send(conn, resp);
            } else {
                resp.setSuccess(true);
                resp.setLobby(lobby);
                for (final PlayerModel other : lobby.getPlayers()) {
                    LOGGER.info("tell");
                    LobbyPlayerJoinMessage joinMsg = new LobbyPlayerJoinMessage();
                    joinMsg.setPlayer(player);
                    send(other.getWebSocket(), joinMsg);
                }
                // Garder cette ligne après la boucle ci-dessus
                lobby.addPlayer(player);
                send(conn, resp);
                LOGGER.info(() -> player + " joined lobby " + lobby.getDescription().getLobbyId());

                // TODO déplacer ça dans un meilleur endroit
                if (lobby.getPlayerCount() == lobby.getDescription().getPlayerLimit()) {
                    validateLobby(lobby);
                }
            }
        }
    }

    private void validateLobby(LobbyModel lobby) {
        LOGGER.info(() -> "Lobby " + lobby.getDescription().getLobbyId() + " validated");
        lobbies.remove(lobby.getDescription().getLobbyId());
        lobby.getSelectableProtoponeys().addAll(Arrays.asList(ProtoPoney.DEFAULTS));
        PoneySelectBeginMessage startMsg = new PoneySelectBeginMessage();
        startMsg.setProtoPoneys(ProtoPoney.DEFAULTS);
        for (PlayerModel player : lobby.getPlayers()) {
            send(player.getWebSocket(), startMsg);
        }
    }

    private void handlePoneySelectUpdate(PlayerModel player, PoneySelectUpdateMessage msg) {
        final LobbyModel lobby = player.getCurrentLobby();
        PoneySelectUpdateMessage resp = new PoneySelectUpdateMessage();
        ProtoPoney select = lobby.getSelectableProtoponeys().get(msg.getPoneyIndex());
        if (msg.getEvent() == PoneySelectUpdateMessage.Event.PONEY_SELECT) {
            if (!lobby.getSelectedProtoponeys().contains(select)) {
                resp.setEvent(PoneySelectUpdateMessage.Event.PONEY_SELECT);
                resp.setPlayerId(player.getPlayerId());
                resp.setPoneyIndex(msg.getPoneyIndex());
                lobby.getSelectedProtoponeys().add(select);
            }
        } else {  // DESELECT
            if (lobby.getSelectedProtoponeys().contains(select)) {
                resp.setEvent(PoneySelectUpdateMessage.Event.PONEY_DESELECT);
                resp.setPlayerId(player.getPlayerId());
                resp.setPoneyIndex(msg.getPoneyIndex());
                lobby.getSelectedProtoponeys().remove(select);
            }
        }
        for (PlayerModel plr : lobby.getPlayers()) {
            send(plr.getWebSocket(), resp);
        }
    }

    /**
     * Gère les messages de <i>leave</i> de lobby.
     */
    private void handleLobbyLeave(PlayerModel player) {
        LobbyModel lobby = player.getCurrentLobby();
        if (lobby != null) {
            lobby.removePlayer(player);
            for (final PlayerModel other : lobby.getPlayers()) {
                LobbyPlayerLeaveMessage leaveMsg = new LobbyPlayerLeaveMessage();
                leaveMsg.setPlayerId(player.getPlayerId());
                send(other.getWebSocket(), leaveMsg);
            }
            LOGGER.info(() -> player + " left lobby " + lobby.getDescription().getLobbyId());
            if (lobby.getPlayerCount() == 0) {
                lobbies.remove(lobby.getDescription().getLobbyId());
                LOGGER.info(() -> "Lobby " + lobby.getDescription().getLobbyId() + " died");
            }
        }
    }

    private void handleLeaderboardRequest(WebSocket conn) throws SQLException {
        LeaderboardModel.Entry[] entries;
        try (Statement st = db.createStatement(
                ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY)) {
            try (ResultSet rs = st.executeQuery(
                    "SELECT identifiant, meilleur_score FROM Joueur " +
                            "ORDER BY meilleur_score DESC;")) {
                rs.last();
                entries = new LeaderboardModel.Entry[rs.getRow()];
                rs.beforeFirst();
                int i = 0;
                while (rs.next()) {
                    LeaderboardModel.Entry entry = new LeaderboardModel.Entry();
                    entry.setPlayerId(rs.getString(1).hashCode());
                    entry.setNickname(rs.getString(1));
                    entry.setScore(rs.getDouble(2));
                    entries[i] = entry;
                    i++;
                }
            }
        }
        LeaderboardResponse resp = new LeaderboardResponse();
        resp.setEntries(entries);
        send(conn, resp);
    }

    @Override
    public void onClose(WebSocket conn, int code, String reason, boolean remote) {
        LOGGER.info(() -> "Goodbye " + sockRemote(conn));
        PlayerModel plrModel = conn.getAttachment();

        if (plrModel.getCurrentLobby() != null) {
            handleLobbyLeave(plrModel);
        }

        plrModel.setWebSocket(null);
        conn.setAttachment(null);
    }

    @Override
    public void onError(WebSocket conn, Exception ex) {
        LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
    }


}
