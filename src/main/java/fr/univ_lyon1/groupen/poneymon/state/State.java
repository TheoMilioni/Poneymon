package fr.univ_lyon1.groupen.poneymon.state;

import fr.univ_lyon1.groupen.poneymon.controller.StateController;
import fr.univ_lyon1.groupen.poneymon.view.StateView;

public class State {
    private StateDefinition mDefinition;
    private StateController mController;
    private StateView mView;

    /**
     * Constructeur de l'état.
     * @param definition Définition utilisée pour la création.
     * @param controller Contrôleur respectif.
     * @param view Vue respective.
     */
    public State(StateDefinition definition, StateController controller, StateView view) {
        this.mDefinition = definition;
        this.mController = controller;
        this.mView = view;
    }

    public String getName() {
        return mDefinition.getName();
    }

    public StateDefinition getDefinition() {
        return mDefinition;
    }

    public void setDefinition(StateDefinition definition) {
        this.mDefinition = definition;
    }

    public <T extends StateController> T getController() {
        // noinspection unchecked
        return (T) mController;
    }

    public void setController(StateController controller) {
        this.mController = controller;
    }

    public <T extends StateView> T  getView() {
        // noinspection unchecked
        return (T) mView;
    }

    public void setView(StateView view) {
        this.mView = view;
    }
}
