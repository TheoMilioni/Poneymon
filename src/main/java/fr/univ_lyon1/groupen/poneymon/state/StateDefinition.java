package fr.univ_lyon1.groupen.poneymon.state;

import fr.univ_lyon1.groupen.poneymon.controller.GameController;
import fr.univ_lyon1.groupen.poneymon.controller.StateController;
import fr.univ_lyon1.groupen.poneymon.controller.nav.CreateOrJoinController;
import fr.univ_lyon1.groupen.poneymon.controller.nav.EndController;
import fr.univ_lyon1.groupen.poneymon.controller.nav.JoinController;
import fr.univ_lyon1.groupen.poneymon.controller.nav.LeaderboardController;
import fr.univ_lyon1.groupen.poneymon.controller.nav.MainMenuController;
import fr.univ_lyon1.groupen.poneymon.controller.nav.MultiConnectionController;
import fr.univ_lyon1.groupen.poneymon.controller.nav.MultiCreateController;
import fr.univ_lyon1.groupen.poneymon.controller.nav.MultiSetupController;
import fr.univ_lyon1.groupen.poneymon.controller.nav.SoloSetupController;
import fr.univ_lyon1.groupen.poneymon.controller.nav.WaitingLobbyController;
import fr.univ_lyon1.groupen.poneymon.view.GameView;
import fr.univ_lyon1.groupen.poneymon.view.StateView;
import fr.univ_lyon1.groupen.poneymon.view.nav.CreateOrJoinView;
import fr.univ_lyon1.groupen.poneymon.view.nav.EndView;
import fr.univ_lyon1.groupen.poneymon.view.nav.JoinView;
import fr.univ_lyon1.groupen.poneymon.view.nav.LeaderboardView;
import fr.univ_lyon1.groupen.poneymon.view.nav.MainMenuView;
import fr.univ_lyon1.groupen.poneymon.view.nav.MultiConnectionView;
import fr.univ_lyon1.groupen.poneymon.view.nav.MultiCreateView;
import fr.univ_lyon1.groupen.poneymon.view.nav.MultiSetupView;
import fr.univ_lyon1.groupen.poneymon.view.nav.SoloSetupView;
import fr.univ_lyon1.groupen.poneymon.view.nav.WaitingLobbyView;

public enum StateDefinition {
    MAIN_MENU("MainMenu", MainMenuController.class, MainMenuView.class),
    LEADERBOARD("Leaderboard", LeaderboardController.class, LeaderboardView.class),
    MULTICONNECTION("MultiConnection", MultiConnectionController.class, MultiConnectionView.class),
    SOLOSETUP("SoloSetup", SoloSetupController.class, SoloSetupView.class),
    WAITIINGLOBBY("WaitingLobby", WaitingLobbyController.class, WaitingLobbyView.class),
    CREATEORJOIN("CreateOrJoin", CreateOrJoinController.class, CreateOrJoinView.class),
    JOIN("Join", JoinController.class, JoinView.class),
    MULTICREATE("MultiCreate", MultiCreateController.class, MultiCreateView.class),
    MULTISETUP("MultiSetup", MultiSetupController.class, MultiSetupView.class),
    GAME("Game", GameController.class, GameView.class),
    END("End", EndController.class, EndView.class);

    private String mName;
    private Class<? extends StateController> mControllerClass;
    private Class<? extends StateView> mViewClass;

    StateDefinition(String name, Class<? extends StateController> controllerClass,
                           Class<? extends StateView> viewClass) {
        mName = name;
        mControllerClass = controllerClass;
        mViewClass = viewClass;
    }

    public String getName() {
        return mName;
    }

    public Class<? extends StateController> getControllerClass() {
        return mControllerClass;
    }

    public Class<? extends StateView> getViewClass() {
        return mViewClass;
    }
}
