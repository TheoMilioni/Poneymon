package fr.univ_lyon1.groupen.poneymon.model;

import fr.univ_lyon1.groupen.poneymon.model.item.Item;
import fr.univ_lyon1.groupen.poneymon.model.item.WallModel;
import fr.univ_lyon1.groupen.poneymon.model.item.EndModel;
import fr.univ_lyon1.groupen.poneymon.model.item.BonusModel;
import fr.univ_lyon1.groupen.poneymon.model.item.IceModel;
import fr.univ_lyon1.groupen.poneymon.model.item.MudModel;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class Race {
    public enum Difficulty {
        EASY("Facile"),
        HARD("Difficile");

        private String name;

        Difficulty(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        @Override
        public String toString() {
            return getName();
        }
    }

    private int raceLength;
    private int poneyCount;
    private Difficulty difficulty;
    private int aiControlledPoneys = 0; //Nombre de poney controllé par une IA
    private List<Item> items;           //Liste des items présent sur la course
    private List<ProtoPoney> protoPoneyList;

    public Race() {
        // Laisser pour que Kryo puisse l'instancier
    }

    /**
     * Initialisation de la course.
     * @param raceLength longeur de la course en mètre (minimun : 20m)
     */
    public Race(int raceLength, Difficulty difficulty, List<ProtoPoney> protoPoneyList) {
        if (raceLength < 20) {
            throw new IllegalArgumentException("Paramètres incorrectes");
        }
        this.poneyCount = protoPoneyList.size();
        this.raceLength = raceLength;
        this.difficulty = difficulty;
        this.protoPoneyList = protoPoneyList;
        items = new ArrayList<>();
    }

    public int getPoneyCount() {
        return poneyCount;
    }

    public int getAiControlledPoneys() {
        return aiControlledPoneys;
    }

    /**
     * Setting du nombre de poneys généré par des IA.
     * @param aiControlledPoneys entier
     */
    public void setAiControlledPoneys(int aiControlledPoneys) {
        if (aiControlledPoneys < 0) {
            throw new IllegalArgumentException("Paramètre incorrect");
        }
        this.aiControlledPoneys = aiControlledPoneys;
    }

    public double getRaceLength() {
        return raceLength;
    }

    public List<Item> getItems() {
        return items;
    }

    public List<ProtoPoney> getProtoPoneyList() {
        return protoPoneyList;
    }

    /**
     * Fonction ajoutant des motif aléatoire à la liste d'item.
     * Les motifs sont codé en dur dans le code
     */
    public void generateMotif() {
        for (int start = 0; start < raceLength - 18; start += 18) {
            int random = 0;
            if (difficulty == Difficulty.EASY) {
                random = 0 + (int)(Math.random() * ((4 - 0) + 1)); //0 à 4
            }
            if (difficulty == Difficulty.HARD) {
                random = 5 + (int)(Math.random() * ((11 - 5) + 1));//5 à 11
            }
            switch (random) {
                case 0 :
                    items.add(new WallModel(start + 6 * 1,1,1));
                    items.add(new IceModel(start + 6 * 2,0,3));
                    items.add(new WallModel(start + 6 * 3,2,1));
                    break;
                case 1 :
                    items.add(new WallModel(start + 6 * 1,1,1));
                    items.add(new MudModel(start + 6 * 2,1,3));
                    items.add(new MudModel(start + 6 * 2,2,2));
                    items.add(new WallModel(start + 6 * 3,0,1));
                    items.add(new BonusModel(start + 6 * 3,1,1));
                    break;
                case 2 :
                    items.add(new WallModel(start + 6 * 1,0,1));
                    items.add(new MudModel(start + 6 * 2,0,3));
                    items.add(new MudModel(start + 6 * 2,1,2));
                    items.add(new WallModel(start + 6 * 3,2,1));
                    break;
                case 3 :
                    items.add(new WallModel(start + 6 * 1,2,1));
                    items.add(new IceModel(start + 6 * 2,0,4));
                    items.add(new WallModel(start + 6 * 3,1,1));
                    items.add(new WallModel(start + 6 * 3,2,1));
                    break;
                case 4 :
                    items.add(new WallModel(start + 6 * 1,1,1));
                    items.add(new BonusModel(start + 6 * 2,1,1));
                    items.add(new MudModel(start + 6 * 2,2,2));
                    items.add(new WallModel(start + 6 * 3,0,1));
                    break;
                case 5 :
                    items.add(new MudModel(start + 6 * 1 + 1, 0, 3));
                    items.add(new MudModel(start + 6 * 1, 1, 3));
                    items.add(new WallModel(start + 6 * 2, 2, 1));
                    items.add(new BonusModel(start + 6 * 2 + 2, 2, 1));
                    items.add(new MudModel(start + 6 * 2 + 2, 1, 3));
                    items.add(new WallModel(start + 6 * 3, 1, 1));
                    break;
                case 6:
                    items.add(new WallModel(start + 6 * 1,0,1));
                    items.add(new WallModel(start + 6 * 1,2,1));
                    items.add(new WallModel(start + 6 * 1 + 3,0,1));
                    items.add(new WallModel(start + 6 * 1 + 3,2,1));
                    items.add(new WallModel(start + 6 * 2,0,1));
                    items.add(new WallModel(start + 6 * 2,2,1));
                    items.add(new WallModel(start + 6 * 2 + 3,0,1));
                    items.add(new WallModel(start + 6 * 2 + 3,2,1));
                    items.add(new WallModel(start + 6 * 3,0,1));
                    items.add(new WallModel(start + 6 * 3,2,1));
                    items.add(new WallModel(start + 6 * 3 + 3,0,1));
                    items.add(new WallModel(start + 6 * 3 + 3,2,1));
                    break;
                case 7:
                    items.add(new WallModel(start + 6 * 1,2,1));
                    items.add(new WallModel(start + 6 * 2,1,1));
                    items.add(new WallModel(start + 6 * 2,2,1));
                    items.add(new WallModel(start + 6 * 3,0,1));
                    items.add(new WallModel(start + 6 * 3,2,1));
                    break;
                case 8:
                    items.add(new WallModel(start + 6 * 1,1,1));
                    items.add(new WallModel(start + 6 * 2,0,1));
                    items.add(new WallModel(start + 6 * 2,1,1));
                    items.add(new WallModel(start + 6 * 3,1,1));
                    items.add(new WallModel(start + 6 * 3,2,1));
                    break;
                case 9:
                    items.add(new WallModel(start + 6 * 1,0,1));
                    items.add(new WallModel(start + 6 * 2,1,1));
                    items.add(new WallModel(start + 6 * 2,2,1));
                    items.add(new WallModel(start + 6 * 3,0,1));
                    items.add(new WallModel(start + 6 * 3,2,1));
                    break;
                case 10:
                    items.add(new WallModel(start + 6 * 1, 0, 1));
                    items.add(new WallModel(start + 6 * 1, 2, 1));
                    items.add(new IceModel(start + 6 * 1 + 3, 1, 3));
                    items.add(new BonusModel(start + 6 * 2 + 2, 1, 1));
                    items.add(new WallModel(start + 6 * 2 + 3, 1, 1));
                    items.add(new IceModel(start + 6 * 3, 0, 3));
                    items.add(new WallModel(start + 6 * 3 + 3, 0, 1));
                    break;
                case 11:
                    items.add(new MudModel(start + 6 * 1 + 1, 0, 3));
                    items.add(new MudModel(start + 6 * 1, 1, 3));
                    items.add(new WallModel(start + 6 * 2, 2, 1));
                    items.add(new BonusModel(start + 6 * 2 + 2, 2, 1));
                    items.add(new MudModel(start + 6 * 2 + 2, 1, 3));
                    items.add(new WallModel(start + 6 * 3, 1, 1));
                    break;
                case 12:
                    items.add(new WallModel(start + 6 * 1,2,1));
                    items.add(new WallModel(start + 6 * 2,0,1));
                    items.add(new WallModel(start + 6 * 2,1,1));
                    items.add(new WallModel(start + 6 * 3,0,1));
                    items.add(new WallModel(start + 6 * 3,2,1));
                    break;
                default :
                    break;
            }
        }
        items.add(new EndModel(raceLength, 0, 2));
        items.add(new EndModel(raceLength, 1, 2));
        items.add(new EndModel(raceLength, 2, 2));
    }

    public Difficulty getDifficulty() {
        return difficulty;
    }
}
