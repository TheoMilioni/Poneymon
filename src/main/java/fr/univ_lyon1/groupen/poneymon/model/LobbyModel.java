package fr.univ_lyon1.groupen.poneymon.model;

import java.util.ArrayList;
import java.util.List;

public class LobbyModel {
    public static class Description {
        private long lobbyId;
        private String name;
        private int playerCount;
        private int playerLimit;
        private boolean passwordProtected;
        private boolean ranked;
        private Race race;
        // Type de terrain, règles de la course, ...

        public long getLobbyId() {
            return lobbyId;
        }

        public void setLobbyId(long lobbyId) {
            this.lobbyId = lobbyId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getPlayerCount() {
            return playerCount;
        }

        public void setPlayerCount(int playerCount) {
            this.playerCount = playerCount;
        }

        public int getPlayerLimit() {
            return playerLimit;
        }

        public void setPlayerLimit(int playerLimit) {
            this.playerLimit = playerLimit;
        }

        public boolean isPasswordProtected() {
            return passwordProtected;
        }

        public void setPasswordProtected(boolean passwordProtected) {
            this.passwordProtected = passwordProtected;
        }

        public boolean isRanked() {
            return ranked;
        }

        public void setRanked(boolean ranked) {
            this.ranked = ranked;
        }

        public Race getRace() {
            return race;
        }

        public void setRace(Race race) {
            this.race = race;
        }

        @Override
        public String toString() {
            return "Description{" +
                    "lobbyId=" + lobbyId +
                    ", name='" + name + '\'' +
                    ", playerCount=" + playerCount +
                    ", playerLimit=" + playerLimit +
                    ", passwordProtected=" + passwordProtected +
                    ", ranked=" + ranked +
                    ", race=" + race +
                    '}';
        }
    }

    private Description description = new Description();
    private List<PlayerModel> players = new ArrayList<>();
    private List<ProtoPoney> selectedProtoponeys = new ArrayList<>();

    @SuppressWarnings("unused")
    public LobbyModel() {
    }

    /**
     * Créé un lobby avec un identifiant et un nom.
     * @param id Identifiant.
     * @param name Nom.
     */
    public LobbyModel(long id, String name, Race race) {
        description.setLobbyId(id);
        description.setName(name);
        description.setPlayerLimit(4);  // TODO don't hardcode
        description.setRace(race);
    }

    public Description getDescription() {
        return description;
    }

    /**
     * Ajout d'un joueur.
     * @param player joueur à ajouter
     */
    public void addPlayer(final PlayerModel player) {
        players.add(player);
        player.setCurrentLobby(this);
        description.setPlayerCount(players.size());
    }

    /**
     * Supprime un joueur du lobby.
     * @param player Joueur à supprimer.
     */
    public void removePlayer(final PlayerModel player) {
        players.remove(player);
        player.setCurrentLobby(null);
        description.setPlayerCount(players.size());
    }

    public List<PlayerModel> getPlayers() {
        return players;
    }

    public int getPlayerCount() {
        return players.size();
    }

    @Override
    public String toString() {
        return "LobbyModel{"
                + "description=" + description
                + ", players=" + players
                + '}';
    }

    public List<ProtoPoney> getSelectableProtoponeys() {
        return description.getRace().getProtoPoneyList();
    }

    public List<ProtoPoney> getSelectedProtoponeys() {
        return selectedProtoponeys;
    }
}
