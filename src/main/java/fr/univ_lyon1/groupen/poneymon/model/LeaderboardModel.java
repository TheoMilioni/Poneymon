package fr.univ_lyon1.groupen.poneymon.model;

public class LeaderboardModel {
    public static class Entry {
        private long playerId;
        private String nickname;
        private double score;

        public long getPlayerId() {
            return playerId;
        }

        public void setPlayerId(long playerId) {
            this.playerId = playerId;
        }

        public String getNickname() {
            return nickname;
        }

        public void setNickname(String nickname) {
            this.nickname = nickname;
        }

        public double getScore() {
            return score;
        }

        public void setScore(double score) {
            this.score = score;
        }

        @Override
        public String toString() {
            return "Entry{" +
                    "playerId=" + playerId +
                    ", nickname='" + nickname + '\'' +
                    ", score=" + score +
                    '}';
        }
    }

    @SuppressWarnings("squid:S1118")
    public LeaderboardModel() {
        // Laissé comme exercice au lecteur.
    }
}
