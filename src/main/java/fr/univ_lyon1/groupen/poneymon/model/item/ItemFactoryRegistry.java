package fr.univ_lyon1.groupen.poneymon.model.item;

import java.util.ArrayList;
import java.util.List;

public abstract class ItemFactoryRegistry {
    private static List<ItemFactory> factories = new ArrayList<>();

    static {
        // Devrait être dans leur classes respectives, mais
        // https://stackoverflow.com/questions/15509337/why-doesnt-my-static-block-of-code-execute
        ItemFactoryRegistry.registerItemFactory(new BonusModel.Factory());
        ItemFactoryRegistry.registerItemFactory(new MudModel.Factory());
        ItemFactoryRegistry.registerItemFactory(new IceModel.Factory());
        ItemFactoryRegistry.registerItemFactory(new WallModel.Factory());
        ItemFactoryRegistry.registerItemFactory(new EndModel.Factory());
    }

    private ItemFactoryRegistry() {}

    static void registerItemFactory(ItemFactory factory) {
        factories.add(factory);
    }

    public static List<ItemFactory> getFactories() {
        return factories;
    }

    /** Obtient une Factory pour l'item au nom donné.
     *
     * @param itemName Nom de l'item que la Factory doit produire.
     * @return Factory de l'item. null si non trouvée.
     */
    public static ItemFactory getFactory(String itemName) {
        for (ItemFactory factory : factories) {
            if (factory.getItemName().equals(itemName)) {
                return factory;
            }
        }
        return null;
    }
}
