package fr.univ_lyon1.groupen.poneymon.model.item;

public interface ItemFactory {
    String getItemName();

    Item create(double x, int lane, double width);

}
