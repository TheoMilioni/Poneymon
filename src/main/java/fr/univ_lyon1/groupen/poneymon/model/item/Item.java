package fr.univ_lyon1.groupen.poneymon.model.item;

import fr.univ_lyon1.groupen.poneymon.model.PoneyModel;

public abstract class Item {
    protected double x;
    protected int lane;
    protected double width;
    protected boolean[] used = new boolean[4];//un pour chaque piste
    protected boolean[] observable = new boolean[4];//un pour chaque piste

    Item(double x, int lane, double width) {
        this.x = x;
        this.lane = lane;
        this.width = width;
        for (int i = 0; i < used.length; i++) {
            used[i] = false;
        }
        for (int i = 0; i < observable.length; i++) {
            observable[i] = true;
        }
    }

    public abstract String getName();

    public double getX() {
        return x;
    }

    public double getLane() {
        return lane;
    }

    public double getWidth() {
        return width;
    }

    /**
     * Retourne si le poney en entrée à utilisé l'item.
     * @param poney un poney
     * @return used
     */
    public boolean getUsed(PoneyModel poney) {
        return used[poney.getTrack()];
    }

    public boolean isObservable(PoneyModel poney) {
        return observable[poney.getTrack()];
    }

    /**
     * Effet lorsque l'item rentre en contact avec l'item.
     * @param poney en contact
     */
    public abstract void startContact(PoneyModel poney);

    /**
     * Effet lorsque le poney sort du contact avec l'item.
     * @param poney en fin de contact
     */
    public abstract void endContact(PoneyModel poney);
}
