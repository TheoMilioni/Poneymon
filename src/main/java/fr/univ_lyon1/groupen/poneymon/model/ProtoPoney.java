package fr.univ_lyon1.groupen.poneymon.model;

public class ProtoPoney {
    @SuppressWarnings("squid:S2386")
    public static final ProtoPoney[] DEFAULTS = new ProtoPoney[] {
        new ProtoPoney(PoneyColor.BLUE, 1, 7, 1, 15, 13, 20),
        new ProtoPoney(PoneyColor.GREEN, 0.6, 7, 1, 15, 11, 36),
        new ProtoPoney(PoneyColor.ORANGE, 1.2, 6, 1, 15, 12, 26),
        new ProtoPoney(PoneyColor.PURPLE, 0.8, 5, 1, 15, 18, 40),
        new ProtoPoney(PoneyColor.YELLOW, 1.8, 9, 1, 15, 10, 10)
    };

    // TODO: name

    private PoneyColor color;

    private double acceleration;
    private double speed;
    private double nerfSpeed;

    private double boostAcceleration;
    private double boostSpeed;
    private double boostDistance;

    /**
     * Constructeur par défaut du {@link ProtoPoney}.
     * Affecte des valeurs par défaut.
     */
    public ProtoPoney() {
        acceleration = 1;
        speed = 10.0;
        nerfSpeed = 1.0;
        boostAcceleration = acceleration * 2;
        boostSpeed = 15.0;
        boostDistance = 10;
    }

    /**
     * Constructeur de {@link ProtoPoney} avec des paramètres personnalisés.
     */
    public ProtoPoney(PoneyColor color, double acceleration, double speed, double nerfSpeed,
                      double boostAcceleration, double boostSpeed, double boostDistance) {
        this.color = color;
        this.acceleration = acceleration;
        this.speed = speed;
        this.nerfSpeed = nerfSpeed;
        this.boostAcceleration = boostAcceleration;
        this.boostSpeed = boostSpeed;
        this.boostDistance = boostDistance;
    }

    public PoneyColor getColor() {
        return color;
    }

    public void setColor(PoneyColor color) {
        this.color = color;
    }

    public double getAcceleration() {
        return acceleration;
    }

    public void setAcceleration(double acceleration) {
        this.acceleration = acceleration;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public double getNerfSpeed() {
        return nerfSpeed;
    }

    public void setNerfSpeed(double nerfSpeed) {
        this.nerfSpeed = nerfSpeed;
    }

    public double getBoostAcceleration() {
        return boostAcceleration;
    }

    public void setBoostAcceleration(double boostAcceleration) {
        this.boostAcceleration = boostAcceleration;
    }

    public double getBoostSpeed() {
        return boostSpeed;
    }

    public void setBoostSpeed(double boostSpeed) {
        this.boostSpeed = boostSpeed;
    }

    public double getBoostDistance() {
        return boostDistance;
    }

    public void setBoostDistance(double boostDistance) {
        this.boostDistance = boostDistance;
    }
}
