package fr.univ_lyon1.groupen.poneymon.model.item;

import fr.univ_lyon1.groupen.poneymon.model.PoneyModel;

/**
 * BonusModel.
 */
public class BonusModel extends Item {

    private static final String NAME = "bonus";

    public static class Factory implements ItemFactory {

        @Override
        public String getItemName() {
            return NAME;
        }

        @Override
        public Item create(double x, int lane, double width) {
            return new BonusModel(x, lane, width);
        }

    }

    public BonusModel(double x, int lane, double width) {
        super(x, lane, width);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void startContact(PoneyModel poney) {
        if (!used[poney.getTrack()]) {
            used[poney.getTrack()] = true;
            poney.setBoostAvailable(true);
        }
        observable[poney.getTrack()] = false;
    }

    @Override
    public void endContact(PoneyModel poney) {
        // Pas d'action
    }
}