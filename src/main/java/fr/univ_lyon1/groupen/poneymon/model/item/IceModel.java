package fr.univ_lyon1.groupen.poneymon.model.item;

import fr.univ_lyon1.groupen.poneymon.model.PoneyModel;

public class IceModel extends Item {
    private static final String NAME = "ice";

    public static class Factory implements ItemFactory {

        @Override
        public String getItemName() {
            return NAME;
        }

        @Override
        public Item create(double x, int lane, double width) {
            return new BonusModel(x, lane, width);
        }

    }

    public IceModel(double x, int lane, double width) {
        super(x, lane, width);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void startContact(PoneyModel poney) {
        used[poney.getTrack()] = true;
        poney.setFreeeze(true);
    }

    @Override
    public void endContact(PoneyModel poney) {
        poney.setFreeeze(false);
    }
}
