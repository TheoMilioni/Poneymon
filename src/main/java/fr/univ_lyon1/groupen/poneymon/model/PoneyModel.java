package fr.univ_lyon1.groupen.poneymon.model;

import fr.univ_lyon1.groupen.poneymon.controller.ai.AI;
import fr.univ_lyon1.groupen.poneymon.util.Event1;
import fr.univ_lyon1.groupen.poneymon.util.Event2;

/**
 * Class gerant un PoneyModel.
 */
public class PoneyModel {
    public static final double MAX_SPEED = 30000 / 3600.; // 30km/h
    public static final double MIN_SPEED = 5000 / 3600.; // 5km/h

    private final ProtoPoney protoPoney;

    /**
     * Total de mètres parcourus par le poney, depuis le début du tour.
     */
    double x;
    /**
     * Vitesse du poney, en m/s.
     */
    double speed;
    /**
     * Vitesse maximale du poney, en m/s.
     */
    private double maxSpeed;
    /**
     * Accélération du poney, en m/s².
     */
    private double acceleration;

    /**
     * Numéro de la piste sur laquelle se trouve le poney.
     */
    private int track;

    /**
     * Nombre de couloirs sur lequel le poney peut circuler.
     */
    public static final int LANE_COUNT = 3;
    /**
     * Numéro du couloir actuel. Compris entre <tt>0</tt> et {@link #LANE_COUNT}.
     */
    private int lane;
    /**
     * Évènement déclenché lors du changement de couloir.
     */
    public final Event2<PoneyModel, Integer, Integer> onLaneChange = new Event2<>(this);
    /**
     * Si le poney ne peut pas changer de couloir, vaut <tt>false</tt>.
     */
    private boolean laneLocked = true;

    /**
     * Si le poney est en train de booster.
     */
    private boolean boost = false;
    /**
     * Si le boost est prêt à être utilisé.
     */
    private boolean boostAvailable = false;
    /**
     * Évènement déclenché lors du boost ou arrêt de boost.
     */
    public final Event1<PoneyModel, Boolean> onBoostChange = new Event1<>(this);
    /**
     * Position à laquelle le poney a commencé à booster. Utile seulement quand
     * {@link #boost} vaut <tt>true</tt>.
     */
    private double boostStartX;

    AI ai;

    private boolean nerf = false;
    private boolean freeeze = false;

    /**
     * Constructeur du PoneyModel.
     */
    PoneyModel(ProtoPoney protoPoney, int track) {
        this.protoPoney = protoPoney;
        this.track = track;
        x = 0.0;
        speed = 0.0;
        maxSpeed = protoPoney.getSpeed();
        acceleration = protoPoney.getAcceleration();
        lane = 1;    //le poney est au centre de la piste par défaut
    }

    /* Course en cours*/

    private Race currentRace;

    public void setCurrentRace(final Race race) {
        currentRace = race;
    }

    public Race getCurrentRace() {
        return currentRace;
    }

    /**
     * Deplacement vers le haut de la piste du poney.
     */
    public void moveUp() {
        if (!laneLocked && lane > 0) {
            --lane;
            onLaneChange.fire(lane + 1, lane);
        }
    }

    /**
     * Deplacement vers le bas de la piste du poney.
     */
    public void moveDown() {
        if (!laneLocked && lane < LANE_COUNT - 1) {
            ++lane;
            onLaneChange.fire(lane - 1, lane);
        }
    }

    public boolean isBoost() {
        return boost;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    /**
     * Déplacement du poney.
     * @param deltaTime nombre de secondes depuis le dernier mouvement
     */
    public void move(double deltaTime) {
        // Poney gelé
        if (freeeze) {
            laneLocked = true;
        } else {
            laneLocked = false;
            if (nerf) {
                maxSpeed = protoPoney.getNerfSpeed();
            } else if (boost) {
                maxSpeed = protoPoney.getBoostSpeed();
                if ((getX() - boostStartX) > protoPoney.getBoostDistance()) {
                    boost = false;
                    acceleration = protoPoney.getAcceleration();
                    maxSpeed = protoPoney.getSpeed();
                    onBoostChange.fire(false);
                }
            } else {
                maxSpeed = protoPoney.getSpeed();
            }
            speed = Math.min(speed + acceleration * deltaTime, maxSpeed);
        }

        x += speed * deltaTime;

    }

    /**
     * Boost du PoneyModel.
     */
    public void boost() {
        if (boostAvailable) {
            boostStartX = getX();
            acceleration = protoPoney.getBoostAcceleration();
            maxSpeed = protoPoney.getBoostSpeed();
            boost = true;
            boostAvailable = false;
            onBoostChange.fire(true);
        }
    }

    public ProtoPoney getProtoPoney() {
        return protoPoney;
    }

    public PoneyColor getColor() {
        return protoPoney.getColor();
    }

    public double getX() {
        return x;
    }

    /** Obtient l'IA attachée à ce poney.
     *
     * @return AI du poney. Peut être null.
     */
    public AI getAI() {
        return ai;
    }

    public void setAI(AI ai) {
        this.ai = ai;
    }

    public int getLane() {
        return lane;
    }

    public double getAcceleration() {
        return acceleration;
    }

    public boolean isNerf() {
        return nerf;
    }

    public void setNerf(boolean nerf) {
        this.nerf = nerf;
    }

    public boolean isFreeeze() {
        return freeeze;
    }

    public void setFreeeze(boolean freeeze) {
        this.freeeze = freeeze;
    }

    public int getTrack() {
        return track;
    }

    public double getMaxSpeed() {
        return maxSpeed;
    }

    public boolean isBoostAvailable() {
        return boostAvailable;
    }

    public void setBoostAvailable(boolean boostAvailable) {
        this.boostAvailable = boostAvailable;
    }
}
