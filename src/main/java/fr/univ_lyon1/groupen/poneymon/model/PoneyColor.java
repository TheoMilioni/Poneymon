package fr.univ_lyon1.groupen.poneymon.model;

import javafx.scene.paint.Color;

public enum PoneyColor {
    BLUE("blue", Color.web("#322AB9"), Color.web("#6348D9"), "assets/pony-blue.png",
            "assets/pony-blue-running.gif", "assets/pony-blue-rainbow.gif"),
    GREEN("green", Color.web("#29C55F"), Color.web("#45E98F"), "assets/pony-green.png",
            "assets/pony-green-running.gif","assets/pony-green-rainbow.gif"),
    ORANGE("orange", Color.web("#DC3218"), Color.web("#FC6930"), "assets/pony-orange.png",
            "assets/pony-orange-running.gif","assets/pony-orange-rainbow.gif"),
    PURPLE("purple", Color.web("#9B29C3"), Color.web("#CC46E4"), "assets/pony-purple.png",
            "assets/pony-purple-running.gif","assets/pony-purple-rainbow.gif"),
    YELLOW("yellow", Color.web("#E5751F"), Color.web("#FDB137"), "assets/pony-yellow.png",
            "assets/pony-yellow-running.gif","assets/pony-yellow-rainbow.gif");

    private String name;
    private Color primaryColor;
    private Color secondaryColor;
    private String normalImagePath;
    private String runningImagePath;
    private String rainbowImagePath;

    @SuppressWarnings("squid:UnusedPrivateMethod")
    PoneyColor(String name, Color primaryColor, Color secondaryColor,
               String normal, String running, String rainbow) {
        this.name = name;
        this.primaryColor = primaryColor;
        this.secondaryColor = secondaryColor;
        this.normalImagePath = normal;
        this.runningImagePath = running;
        this.rainbowImagePath = rainbow;
    }

    public String getName() {
        return name;
    }

    public Color getPrimaryColor() {
        return primaryColor;
    }

    public Color getSecondaryColor() {
        return secondaryColor;
    }

    public String getNormalImagePath() {
        return normalImagePath;
    }

    public String getRunningImagePath() {
        return runningImagePath;
    }

    public String getRainbowImagePath() {
        return rainbowImagePath;
    }

    @Override
    public String toString() {
        return "PoneyColor{" +
                "name='" + name + '\'' +
                ", primaryColor=" + primaryColor +
                ", secondaryColor=" + secondaryColor +
                ", normalImagePath='" + normalImagePath + '\'' +
                ", runningImagePath='" + runningImagePath + '\'' +
                ", rainbowImagePath='" + rainbowImagePath + '\'' +
                '}';
    }
}
