package fr.univ_lyon1.groupen.poneymon.model;

import fr.univ_lyon1.groupen.poneymon.util.ServerSide;
import org.java_websocket.WebSocket;

public class PlayerModel {
    private long playerId;
    private String nickname;

    @ServerSide
    private WebSocket webSocket;
    @ServerSide
    private LobbyModel currentLobby;

    public long getPlayerId() {
        return playerId;
    }

    public void setPlayerId(long playerId) {
        this.playerId = playerId;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    @ServerSide
    public WebSocket getWebSocket() {
        return webSocket;
    }

    @ServerSide
    public void setWebSocket(WebSocket webSocket) {
        this.webSocket = webSocket;
    }

    @ServerSide
    public LobbyModel getCurrentLobby() {
        return currentLobby;
    }

    @ServerSide
    public void setCurrentLobby(LobbyModel currentLobby) {
        this.currentLobby = currentLobby;
    }
}
