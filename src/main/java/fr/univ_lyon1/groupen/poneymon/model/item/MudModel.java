package fr.univ_lyon1.groupen.poneymon.model.item;

import fr.univ_lyon1.groupen.poneymon.model.PoneyModel;

public class MudModel extends Item {
    private static final String NAME = "mud";

    public static class Factory implements ItemFactory {

        @Override
        public String getItemName() {
            return NAME;
        }

        @Override
        public Item create(double x, int lane, double width) {
            return new MudModel(x, lane, width);
        }

    }

    public MudModel(double x, int lane, double width) {
        super(x, lane, width);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void startContact(PoneyModel poney) {
        used[poney.getTrack()] = true;
        poney.setNerf(true);
    }

    @Override
    public void endContact(PoneyModel poney) {
        poney.setNerf(false);
    }
}
