package fr.univ_lyon1.groupen.poneymon.model.item;

import fr.univ_lyon1.groupen.poneymon.model.PoneyModel;

public class WallModel extends Item {
    private static final String NAME = "wall";

    public static class Factory implements ItemFactory {

        @Override
        public String getItemName() {
            return NAME;
        }

        @Override
        public Item create(double x, int lane, double width) {
            return new BonusModel(x, lane, width);
        }

    }

    public WallModel(double x, int lane, double width) {
        super(x, lane, width);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void startContact(PoneyModel poney) {
        if (!used[poney.getTrack()]) {
            used[poney.getTrack()] = true;
            poney.setSpeed(0);
        }
        observable[poney.getTrack()] = false;
        poney.setFreeeze(false);
    }

    @Override
    public void endContact(PoneyModel poney) {
        // Pas d'action
    }
}
