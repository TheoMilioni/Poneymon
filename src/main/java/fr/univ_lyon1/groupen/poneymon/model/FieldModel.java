package fr.univ_lyon1.groupen.poneymon.model;

import fr.univ_lyon1.groupen.poneymon.model.item.Item;

import java.util.List;

/** Modèle du terrain de course.
 */
public class FieldModel {

    /** Poneys. */
    private PoneyModel[] poneys;

    private Race currentRace;
    private PoneyModel winner;
    private double winningTime;

    /** Obtient les paramètres de la course ayant lieu sur le terrain.
     *
     * @return Course en cours.
     */
    public Race getCurrentRace() {
        return currentRace;
    }

    /** Constructeur du terrain de course.
     *
     * @param race Paramètres de la course à jouer.
     */
    public FieldModel(final Race race) {
        currentRace = race;
        if (race.getPoneyCount() < 0 || race.getPoneyCount() > 5) {
            throw new IllegalArgumentException("Too few or too many Poneys in the race");
        }
        poneys = new PoneyModel[race.getPoneyCount()];

        /* On initialise le terrain de course */
        for (int i = 0; i < poneys.length; i++) {
            poneys[i] = new PoneyModel(race.getProtoPoneyList().get(i), i);
            poneys[i].setCurrentRace(race);
        }
    }

    /** Obtient le i-ème PoneyModel présent sur le terrain.
     *
     * @param i Indice du poney.
     * @return Poney à l'indice i.
     */
    public PoneyModel getPoney(int i) {
        return poneys[i];
    }

    /** Obtient la liste des PoneyModel sur le terrain.
     *
     * @return Liste des poneys.
     */
    public PoneyModel[] getPoneys() {
        return poneys;
    }

    public int getPoneyCount() {
        return poneys.length;
    }

    public void setWinningPoney(final PoneyModel winner) {
        this.winner = winner;
        this.winningTime = System.currentTimeMillis();
    }

    public PoneyModel getWinningPoney() {
        return winner;
    }

    public double getWinningTime() {
        return winningTime;
    }

    public List<Item> getItem() {
        return currentRace.getItems();
    }
}
