package fr.univ_lyon1.groupen.poneymon.net.msg;

public class GamePickupMessage extends GamePlayerMessage implements Message {
    // TODO item

    @Override
    public MessageType getType() {
        return MessageType.GAME_ITEM_PICKUP;
    }
}
