package fr.univ_lyon1.groupen.poneymon.net.msg;

public class LoginResponse implements Message {
    private long playerId;
    private boolean success;
    private String message;
    private String sessionToken;

    public long getPlayerId() {
        return playerId;
    }

    public void setPlayerId(long playerId) {
        this.playerId = playerId;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSessionToken() {
        return sessionToken;
    }

    public void setSessionToken(String sessionToken) {
        this.sessionToken = sessionToken;
    }

    @Override
    public MessageType getType() {
        return MessageType.LOGIN_RESPONSE;
    }
}
