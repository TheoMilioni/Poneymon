package fr.univ_lyon1.groupen.poneymon.net;

import com.esotericsoftware.kryo.io.Output;
import fr.univ_lyon1.groupen.poneymon.net.msg.Message;
import org.java_websocket.WebSocket;
import org.java_websocket.server.WebSocketServer;

import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class ServerBase extends WebSocketServer {
    private static final Logger LOGGER = Logger.getLogger(ServerBase.class.getName());

    protected ServerBase(int port) {
        super(new InetSocketAddress(port));
    }

    protected static String sockRemote(final WebSocket conn) {
        InetSocketAddress addr = conn.getRemoteSocketAddress();
        return addr.getAddress().getHostAddress() + ':' + addr.getPort();
    }

    protected void send(WebSocket conn, final Message msg) {
        Output messageOutput = new Output(1024, -1);
        messageOutput.write(new byte[] { (byte) msg.getType().getTypeID(), 0, 0, 0 });
        Kryotik.getKryo().writeObject(messageOutput, msg);
        conn.send(messageOutput.toBytes());
    }

    @Override
    public final void onMessage(WebSocket conn, String message) {
        // On ne gère pas les messages texte
    }

    @Override
    public final void onMessage(WebSocket conn, ByteBuffer messageData) {
        Message msg = Network.parseMessage(messageData);
        if (msg != null) {
            try {
                onMessage(conn, msg);
            } catch (SQLException e) {
                LOGGER.log(Level.SEVERE, e.getMessage(), e);
            }
        }
    }

    protected abstract void onMessage(WebSocket conn, Message msg) throws SQLException;
}
