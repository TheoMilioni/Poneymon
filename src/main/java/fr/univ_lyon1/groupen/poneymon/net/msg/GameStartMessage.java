package fr.univ_lyon1.groupen.poneymon.net.msg;

public class GameStartMessage implements Message {
    private long lobbyId;
    private long gameId;
    private double terrainLength;
    // TODO obstacles
    // TODO items
    // TODO qui a quel Protoponey™


    public long getLobbyId() {
        return lobbyId;
    }

    public void setLobbyId(long lobbyId) {
        this.lobbyId = lobbyId;
    }

    public long getGameId() {
        return gameId;
    }

    public void setGameId(long gameId) {
        this.gameId = gameId;
    }

    public double getTerrainLength() {
        return terrainLength;
    }

    public void setTerrainLength(double terrainLength) {
        this.terrainLength = terrainLength;
    }

    @Override
    public MessageType getType() {
        return MessageType.GAME_START;
    }
}
