package fr.univ_lyon1.groupen.poneymon.net.msg;

public class LobbyPlayerLeaveMessage implements Message {
    private long playerId;

    public long getPlayerId() {
        return playerId;
    }

    public void setPlayerId(long playerId) {
        this.playerId = playerId;
    }

    @Override
    public MessageType getType() {
        return MessageType.LOBBY_PLAYER_LEAVE;
    }
}
