package fr.univ_lyon1.groupen.poneymon.net.msg;

import fr.univ_lyon1.groupen.poneymon.model.LobbyModel;

public class LobbyJoinResponse implements Message {
    private boolean success;
    private String message;
    private LobbyModel lobby;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public LobbyModel getLobby() {
        return lobby;
    }

    public void setLobby(LobbyModel lobby) {
        this.lobby = lobby;
    }

    @Override
    public MessageType getType() {
        return MessageType.LOBBY_JOIN_RESPONSE;
    }
}
