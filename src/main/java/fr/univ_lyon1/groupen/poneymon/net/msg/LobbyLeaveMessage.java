package fr.univ_lyon1.groupen.poneymon.net.msg;

public class LobbyLeaveMessage implements Message {
    @Override
    public MessageType getType() {
        return MessageType.LOBBY_LEAVE;
    }
}
