package fr.univ_lyon1.groupen.poneymon.net.msg;

public interface Message {
    /**
     * @return Type du message.
     */
    MessageType getType();
}
