package fr.univ_lyon1.groupen.poneymon.net.msg;

public class LobbyListRequest implements Message {
    // Options de filtrage ?

    @Override
    public MessageType getType() {
        return MessageType.LOBBY_LIST_REQUEST;
    }
}
