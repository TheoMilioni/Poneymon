package fr.univ_lyon1.groupen.poneymon.net;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.SerializerFactory;
import fr.univ_lyon1.groupen.poneymon.model.LeaderboardModel;
import fr.univ_lyon1.groupen.poneymon.model.LobbyModel;
import fr.univ_lyon1.groupen.poneymon.model.PlayerModel;
import fr.univ_lyon1.groupen.poneymon.model.PoneyColor;
import fr.univ_lyon1.groupen.poneymon.model.ProtoPoney;
import fr.univ_lyon1.groupen.poneymon.model.Race;
import fr.univ_lyon1.groupen.poneymon.net.msg.Message;
import fr.univ_lyon1.groupen.poneymon.net.msg.MessageType;
import fr.univ_lyon1.groupen.poneymon.util.ClientSide;
import fr.univ_lyon1.groupen.poneymon.util.FieldAnnotationAwareSerializer;
import fr.univ_lyon1.groupen.poneymon.util.KryoRegister;
import fr.univ_lyon1.groupen.poneymon.util.ServerSide;

import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/**
 * Classe gérant l'initialisation et le singleton de Kryo, utilisé pour encoder et décoder
 * des objets Java pour les envoyer au travers du réseau.
 */
public abstract class Kryotik {
    private static Kryo kryo;

    private Kryotik() {
    }

    /**
     * Retourne le singleton de Kryo.
     */
    public static Kryo getKryo() {
        if (kryo == null) {
            kryo = new Kryo();
            Set<Class<? extends Annotation>> marks = new HashSet<>();
            marks.add(ServerSide.class);
            marks.add(ClientSide.class);
            SerializerFactory disregardingFactory = new FieldAnnotationAwareSerializer.Factory(
                    marks, true);
            kryo.setDefaultSerializer(disregardingFactory);
            registerMessages(kryo);
            registerModels(kryo);
        }
        return kryo;
    }

    /**
     * Configure Kryo pour qu'il puisse serialiser les messages.
     */
    private static void registerMessages(final Kryo kryo) {
        for (MessageType msgType : MessageType.values()) {
            if (msgType.getTypeClass() != null) {
                Class<? extends Message> msgClass = msgType.getTypeClass();
                kryo.register(msgClass, msgType.getTypeID() + 32);
                for (Class<?> subclass : msgClass.getDeclaredClasses()) {
                    if (subclass.getAnnotation(KryoRegister.class) != null) {
                        kryo.register(subclass);
                    }
                }
            }
        }
    }

    private static void registerModels(final Kryo kryo) {
        final Class<?>[] registerList = new Class<?>[] {
            ArrayList.class,
            LobbyModel.Description.class,
            LobbyModel.Description[].class,
            LobbyModel.class,
            PlayerModel.class,
            LeaderboardModel.Entry.class,
            LeaderboardModel.Entry[].class,
            ProtoPoney.class,
            ProtoPoney[].class,
            PoneyColor.class,
            Race.class,
            Race.Difficulty.class
        };
        for (final Class<?> clazz : registerList) {
            kryo.register(clazz);
        }
    }
}
