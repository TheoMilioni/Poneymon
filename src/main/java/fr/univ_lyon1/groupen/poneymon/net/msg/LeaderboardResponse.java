package fr.univ_lyon1.groupen.poneymon.net.msg;

import fr.univ_lyon1.groupen.poneymon.model.LeaderboardModel;

public class LeaderboardResponse implements Message {
    private LeaderboardModel.Entry[] entries;

    public LeaderboardModel.Entry[] getEntries() {
        return entries;
    }

    public void setEntries(LeaderboardModel.Entry[] entries) {
        this.entries = entries;
    }

    @Override
    public MessageType getType() {
        return MessageType.LEADERBOARD_RESPONSE;
    }
}
