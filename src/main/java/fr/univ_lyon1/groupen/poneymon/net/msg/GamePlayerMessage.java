package fr.univ_lyon1.groupen.poneymon.net.msg;

public abstract class GamePlayerMessage {
    private long playerId;

    public long getPlayerId() {
        return playerId;
    }

    public void setPlayerId(long playerId) {
        this.playerId = playerId;
    }
}
