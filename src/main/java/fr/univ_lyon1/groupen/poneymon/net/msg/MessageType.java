package fr.univ_lyon1.groupen.poneymon.net.msg;

import java.util.HashMap;
import java.util.Map;

public enum MessageType {
    NULL(0, null),
    DEBUG(1, DebugMessage.class),

    LOGIN_REQUEST(2, LoginRequest.class),
    LOGIN_RESPONSE(3, LoginResponse.class),

    LOBBY_LIST_REQUEST(4, LobbyListRequest.class),
    LOBBY_LIST_RESPONSE(5, LobbyListResponse.class),

    LOBBY_JOIN_REQUEST(6, LobbyJoinRequest.class),
    LOBBY_JOIN_RESPONSE(7, LobbyJoinResponse.class),
    LOBBY_PLAYER_JOIN(8, LobbyPlayerJoinMessage.class),
    LOBBY_PLAYER_LEAVE(12, LobbyPlayerLeaveMessage.class),
    // Peut-être LOBBY_CHAT ?
    LOBBY_LEAVE(9, LobbyLeaveMessage.class),

    PONEY_SELECT_BEGIN(10, PoneySelectBeginMessage.class),
    PONEY_SELECT_UPDATE(11, PoneySelectUpdateMessage.class),

    GAME_START(14, GameStartMessage.class),
    GAME_LANE_CHANGE(15, GameLaneChangeMessage.class),
    GAME_ITEM_PICKUP(16, GamePickupMessage.class),
    // On réserve "GAME_ITEM_USE(17, GameItemUseMessage.class)" pour les bonus activables plus tard
    GAME_END(18, GameEndMessage.class),

    LEADERBOARD_REQUEST(19, LeaderboardRequest.class),
    LEADERBOARD_RESPONSE(20, LeaderboardResponse.class);

    private int mTypeID;
    private Class<? extends Message> mClass;

    MessageType(int typeID, Class<? extends Message> clazz) {
        mTypeID = typeID;
        mClass = clazz;
    }

    public int getTypeID() {
        return mTypeID;
    }

    public Class<? extends Message> getTypeClass() {
        return mClass;
    }

    private static final Map<Integer, MessageType> TYPE_MAP = new HashMap<>();
    static {
        for (MessageType msgType : values()) {
            TYPE_MAP.put(msgType.mTypeID, msgType);
        }
    }

    public static MessageType fromTypeID(int typeID) {
        return TYPE_MAP.get(typeID);
    }
}
