package fr.univ_lyon1.groupen.poneymon.net.msg;

public class LeaderboardRequest implements Message {
    @Override
    public MessageType getType() {
        return MessageType.LEADERBOARD_REQUEST;
    }
}
