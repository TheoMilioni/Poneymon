package fr.univ_lyon1.groupen.poneymon.net.msg;

import fr.univ_lyon1.groupen.poneymon.model.LobbyModel;
import fr.univ_lyon1.groupen.poneymon.model.Race;

public class LobbyJoinRequest implements Message {
    private long lobbyId;
    private String password;
    private LobbyModel lobbyTemplate;

    public LobbyJoinRequest() {
    }

    public LobbyJoinRequest(long lobbyId) {
        this.lobbyId = lobbyId;
    }

    public long getLobbyId() {
        return lobbyId;
    }

    public void setLobbyId(long lobbyId) {
        this.lobbyId = lobbyId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public LobbyModel getLobbyTemplate() {
        return lobbyTemplate;
    }

    public void setLobbyTemplate(LobbyModel lobbyTemplate) {
        this.lobbyTemplate = lobbyTemplate;
    }

    @Override
    public MessageType getType() {
        return MessageType.LOBBY_JOIN_REQUEST;
    }
}
