package fr.univ_lyon1.groupen.poneymon.net.msg;

import fr.univ_lyon1.groupen.poneymon.model.PlayerModel;

public class LobbyPlayerJoinMessage implements Message {
    private PlayerModel player;

    public PlayerModel getPlayer() {
        return player;
    }

    public void setPlayer(PlayerModel player) {
        this.player = player;
    }

    @Override
    public MessageType getType() {
        return MessageType.LOBBY_PLAYER_JOIN;
    }
}
