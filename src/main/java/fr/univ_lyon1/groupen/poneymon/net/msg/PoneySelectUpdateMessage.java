package fr.univ_lyon1.groupen.poneymon.net.msg;

import fr.univ_lyon1.groupen.poneymon.util.KryoRegister;

/**
 * Message indiquant la sélection ou déselection d'un type de poney avant la course.
 * Sert à la fois à indiquer ce que les autres joueurs choisissent et à
 * confirmer ou non la sélection du joueur.
 */
public class PoneySelectUpdateMessage implements Message {
    @KryoRegister
    public enum Event {
        PONEY_SELECT,
        PONEY_DESELECT
    }

    private Event event;
    private long playerId;
    private int poneyIndex;

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public long getPlayerId() {
        return playerId;
    }

    public void setPlayerId(long playerId) {
        this.playerId = playerId;
    }

    public int getPoneyIndex() {
        return poneyIndex;
    }

    public void setPoneyIndex(int poneyIndex) {
        this.poneyIndex = poneyIndex;
    }

    @Override
    public MessageType getType() {
        return MessageType.PONEY_SELECT_UPDATE;
    }
}
