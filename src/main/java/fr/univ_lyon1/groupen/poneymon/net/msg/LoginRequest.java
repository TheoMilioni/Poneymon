package fr.univ_lyon1.groupen.poneymon.net.msg;

public class LoginRequest implements Message {
    private boolean createIfNonexistent = false;
    private String username;
    private String password;

    public boolean getCreateIfNonexistent() {
        return createIfNonexistent;
    }

    public void setCreateIfNonexistent(boolean createIfNonexistent) {
        this.createIfNonexistent = createIfNonexistent;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public MessageType getType() {
        return MessageType.LOGIN_REQUEST;
    }
}
