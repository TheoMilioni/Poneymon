package fr.univ_lyon1.groupen.poneymon.net;

import com.esotericsoftware.kryo.io.Input;
import fr.univ_lyon1.groupen.poneymon.net.msg.Message;
import fr.univ_lyon1.groupen.poneymon.net.msg.MessageType;

import java.nio.ByteBuffer;

public abstract class Network {
    public static final int DEFAULT_PORT = 60431;

    private Network() {
    }

    static Message parseMessage(ByteBuffer messageData) {
        final byte[] messageBytes = messageData.array();
        MessageType msgType = MessageType.fromTypeID(messageBytes[0]);
        if (msgType == MessageType.NULL) {
            return null;
        }
        return Kryotik.getKryo().readObject(
                new Input(messageBytes, 4, messageBytes.length - 4), msgType.getTypeClass());
    }
}
