package fr.univ_lyon1.groupen.poneymon.net;

import com.esotericsoftware.kryo.io.Output;
import fr.univ_lyon1.groupen.poneymon.net.msg.Message;
import org.java_websocket.client.WebSocketClient;

import java.net.URI;
import java.nio.ByteBuffer;

public abstract class ClientBase extends WebSocketClient {
    protected ClientBase(URI serverUri) {
        super(serverUri);
    }

    protected void send(final Message msg) {
        Output messageOutput = new Output(1024, -1);
        messageOutput.write(new byte[] { (byte) msg.getType().getTypeID(), 0, 0, 0 });
        Kryotik.getKryo().writeObject(messageOutput, msg);
        send(messageOutput.toBytes());
    }

    @Override
    public final void onMessage(String message) {
        // On ne gère pas les messages texte
    }

    @Override
    public final void onMessage(ByteBuffer messageData) {
        Message msg = Network.parseMessage(messageData);
        if (msg != null) {
            onMessage(msg);
        }
    }

    protected abstract void onMessage(Message msg);
}
