package fr.univ_lyon1.groupen.poneymon.net.msg;

import fr.univ_lyon1.groupen.poneymon.util.KryoRegister;

public class GameLaneChangeMessage extends GamePlayerMessage implements Message {
    @KryoRegister
    public enum ChangeDirection {
        UP,
        DOWN
    }

    private ChangeDirection changeDirection;

    public ChangeDirection getChangeDirection() {
        return changeDirection;
    }

    public void setChangeDirection(ChangeDirection changeDirection) {
        this.changeDirection = changeDirection;
    }

    @Override
    public MessageType getType() {
        return MessageType.GAME_LANE_CHANGE;
    }
}
