package fr.univ_lyon1.groupen.poneymon.net.msg;

import fr.univ_lyon1.groupen.poneymon.model.ProtoPoney;

public class PoneySelectBeginMessage implements Message {
    private ProtoPoney[] protoPoneys;

    public ProtoPoney[] getProtoPoneys() {
        return protoPoneys;
    }

    public void setProtoPoneys(ProtoPoney[] protoPoneys) {
        this.protoPoneys = protoPoneys;
    }

    @Override
    public MessageType getType() {
        return MessageType.PONEY_SELECT_BEGIN;
    }
}
