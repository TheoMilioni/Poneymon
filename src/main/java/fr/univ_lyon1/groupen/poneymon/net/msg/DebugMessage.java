package fr.univ_lyon1.groupen.poneymon.net.msg;

public class DebugMessage implements Message {
    private String debugText;

    public String getDebugText() {
        return debugText;
    }

    public void setDebugText(String debugText) {
        this.debugText = debugText;
    }

    @Override
    public MessageType getType() {
        return MessageType.DEBUG;
    }
}
