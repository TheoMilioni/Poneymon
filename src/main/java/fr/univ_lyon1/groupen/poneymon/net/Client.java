package fr.univ_lyon1.groupen.poneymon.net;

import fr.univ_lyon1.groupen.poneymon.model.LeaderboardModel;
import fr.univ_lyon1.groupen.poneymon.model.LobbyModel;
import fr.univ_lyon1.groupen.poneymon.model.PlayerModel;
import fr.univ_lyon1.groupen.poneymon.model.ProtoPoney;
import fr.univ_lyon1.groupen.poneymon.model.Race;
import fr.univ_lyon1.groupen.poneymon.net.msg.DebugMessage;
import fr.univ_lyon1.groupen.poneymon.net.msg.LeaderboardRequest;
import fr.univ_lyon1.groupen.poneymon.net.msg.LeaderboardResponse;
import fr.univ_lyon1.groupen.poneymon.net.msg.LobbyJoinRequest;
import fr.univ_lyon1.groupen.poneymon.net.msg.LobbyJoinResponse;
import fr.univ_lyon1.groupen.poneymon.net.msg.LobbyLeaveMessage;
import fr.univ_lyon1.groupen.poneymon.net.msg.LobbyListRequest;
import fr.univ_lyon1.groupen.poneymon.net.msg.LobbyListResponse;
import fr.univ_lyon1.groupen.poneymon.net.msg.LobbyPlayerJoinMessage;
import fr.univ_lyon1.groupen.poneymon.net.msg.LobbyPlayerLeaveMessage;
import fr.univ_lyon1.groupen.poneymon.net.msg.LoginRequest;
import fr.univ_lyon1.groupen.poneymon.net.msg.LoginResponse;
import fr.univ_lyon1.groupen.poneymon.net.msg.Message;
import fr.univ_lyon1.groupen.poneymon.net.msg.MessageType;
import fr.univ_lyon1.groupen.poneymon.net.msg.PoneySelectBeginMessage;
import fr.univ_lyon1.groupen.poneymon.net.msg.PoneySelectUpdateMessage;
import org.java_websocket.handshake.ServerHandshake;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Client extends ClientBase {
    private static final Logger LOGGER = Logger.getLogger(ClientBase.class.getName());

    private static Client instance = null;

    /**
     * Récupère l'instance du Client permettant l'interaction avec le serveur.
     * @return Instance de {@link Client}.
     */
    public static Client getInstance() {
        if (instance == null) {
            try {
                instance = new Client(
                        new URI("ws", null, "localhost", Network.DEFAULT_PORT, null, null, null));
                instance.connect();
            } catch (URISyntaxException e) {
                LOGGER.log(Level.SEVERE, e.getMessage(), e);
            }
        }
        return instance;
    }

    /**
     * Point d'entrée de la boucle client.
     */
    public static void main(String[] args) throws URISyntaxException {
        int port = Network.DEFAULT_PORT;
        Client c = new Client(new URI("ws", null, "localhost", port, null, null, null));
        c.setLobbyPlayerJoinCallback(
            player -> LOGGER.info(player + " joined lobby"));
        c.setLobbyPlayerLeaveCallback(
            playerId -> LOGGER.info(playerId + " left lobby"));
        c.setPoneySelectBeginCallback(
            protoPoneys -> LOGGER.info("Poney select begin" + Arrays.toString(protoPoneys)));
        c.connect();
        c.requestLogin(new LoginCallbacks() {
            @Override
            public void onLoginSuccess(long playerId, String token) {
                LOGGER.info(() -> "Logged in as " + playerId + ", token=" + token);
            }

            @Override
            public void onLoginFail(String message) {
                LOGGER.info(() -> "Login failed: " + message);
            }
        }, "abc", "def", true);
        c.requestLobbyList(lobbies -> {
            for (final LobbyModel.Description desc : lobbies) {
                LOGGER.info(desc.toString());
            }
            long lobbyId = 0;
            if (lobbies.length > 0) {
                lobbyId = lobbies[0].getLobbyId();
            }
            c.requestLobbyJoin(lobbyId, new LobbyJoinCallbacks() {
                @Override
                public void onLobbyJoinFailed(String message) {
                    LOGGER.info(message);
                }

                @Override
                public void onLobbyJoin(LobbyModel lobby) {
                    LOGGER.info(lobby.toString());
                }
            });
        });
        c.requestLeaderboard(entries -> {
            for (LeaderboardModel.Entry entry : entries) {
                LOGGER.info(entry.toString());
            }
        });
    }

    public Client(URI serverUri) {
        super(serverUri);
    }

    @Override
    public void onOpen(ServerHandshake handshakedata) {
        LOGGER.info("onOpen");
    }

    @Override
    protected void onMessage(Message msg) {
        MessageType msgType = msg.getType();
        switch (msgType) {
            case DEBUG:
                DebugMessage dmsg = (DebugMessage) msg;
                LOGGER.info(() -> "Debug message: " + dmsg.getDebugText());
                break;
            case LOGIN_RESPONSE:
                handleLoginResponse((LoginResponse) msg);
                break;
            case LOBBY_LIST_RESPONSE:
                handleLobbyListResponse((LobbyListResponse) msg);
                break;
            case LOBBY_JOIN_RESPONSE:
                handleLobbyJoinResponse((LobbyJoinResponse) msg);
                break;
            case LOBBY_PLAYER_JOIN:
                handlePlayerJoinMessage((LobbyPlayerJoinMessage) msg);
                break;
            case LOBBY_PLAYER_LEAVE:
                handlePlayerLeaveMessage((LobbyPlayerLeaveMessage) msg);
                break;
            case LEADERBOARD_RESPONSE:
                handleLeaderboardResponse((LeaderboardResponse) msg);
                break;
            case PONEY_SELECT_BEGIN:
                handlePoneySelectBegin((PoneySelectBeginMessage) msg);
                break;
            case PONEY_SELECT_UPDATE:
                handlePoneySelectUpdate((PoneySelectUpdateMessage) msg);
                break;
            default:
                break;
        }
    }

    @Override
    public void onClose(int code, String reason, boolean remote) {
        LOGGER.info("bye");
    }

    @Override
    public void onError(Exception ex) {
        LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
    }

    // Login

    public interface LoginCallbacks {
        void onLoginSuccess(long playerId, String token);

        void onLoginFail(String message);
    }

    private LoginCallbacks loginCallbacks;

    private void handleLoginResponse(LoginResponse msg) {
        if (loginCallbacks != null) {
            if (msg.isSuccess()) {
                loginCallbacks.onLoginSuccess(msg.getPlayerId(), msg.getSessionToken());
            } else {
                loginCallbacks.onLoginFail(msg.getMessage());
            }
            loginCallbacks = null;
        }
    }

    /**
     * Requête une connexion à un compte.
     * @param callbacks Callbacks à exécuter.
     * @param username Nom d'utilisateur.
     * @param password Mot de passe, en clair.
     * @param createAccount S'il faut créer le compte s'il n'existe pas.
     */
    public void requestLogin(LoginCallbacks callbacks, String username, String password,
                             boolean createAccount) {
        loginCallbacks = callbacks;
        LoginRequest req = new LoginRequest();
        req.setUsername(username);
        req.setPassword(password);
        req.setCreateIfNonexistent(createAccount);
        send(req);
    }

    // Lobby list

    public interface LobbyListCallback {
        void onLobbyListReceived(LobbyModel.Description[] lobbies);
    }

    private LobbyListCallback lobbyListCallback;

    private void handleLobbyListResponse(LobbyListResponse msg) {
        if (lobbyListCallback != null) {
            lobbyListCallback.onLobbyListReceived(msg.getLobbiesDescriptions());
            lobbyListCallback = null;
        }
    }

    public void requestLobbyList(LobbyListCallback callbacks) {
        lobbyListCallback = callbacks;
        send(new LobbyListRequest());
    }

    // Lobby join / create

    public interface LobbyJoinCallbacks {
        void onLobbyJoinFailed(String message);

        void onLobbyJoin(LobbyModel lobby);
    }

    private LobbyJoinCallbacks lobbyJoinCallbacks;

    private void handleLobbyJoinResponse(LobbyJoinResponse msg) {
        if (lobbyJoinCallbacks != null) {
            if (msg.isSuccess()) {
                lobbyJoinCallbacks.onLobbyJoin(msg.getLobby());
            } else {
                lobbyJoinCallbacks.onLobbyJoinFailed(msg.getMessage());
            }
            lobbyJoinCallbacks = null;
        }
    }

    public void requestLobbyJoin(long lobbyId, LobbyJoinCallbacks callbacks) {
        lobbyJoinCallbacks = callbacks;
        send(new LobbyJoinRequest(lobbyId));
    }

    /**
     * Requête la création d'un lobby.
     * @param callbacks Callbacks à exécuter lors de la création du lobby, ou
     *                  l'échec de l'opération.
     * @param lobbyTemplate Modèle du lobby.
     */
    public void requestLobbyCreate(LobbyModel lobbyTemplate, LobbyJoinCallbacks callbacks) {
        LobbyJoinRequest req = new LobbyJoinRequest(0);
        req.setLobbyTemplate(lobbyTemplate);
        lobbyJoinCallbacks = callbacks;
        send(req);
    }

    // Lobby player join

    public interface LobbyPlayerJoinCallback {
        void onLobbyPlayerJoin(PlayerModel player);
    }

    private LobbyPlayerJoinCallback lobbyPlayerJoinCallback;

    private void handlePlayerJoinMessage(LobbyPlayerJoinMessage msg) {
        if (lobbyPlayerJoinCallback != null) {
            lobbyPlayerJoinCallback.onLobbyPlayerJoin(msg.getPlayer());
        }
    }

    public void setLobbyPlayerJoinCallback(LobbyPlayerJoinCallback callback) {
        lobbyPlayerJoinCallback = callback;
    }

    // Lobby player leave

    public interface LobbyPlayerLeaveCallback {
        void onLobbyPlayerLeave(long playerId);
    }

    private LobbyPlayerLeaveCallback lobbyPlayerLeaveCallback;

    private void handlePlayerLeaveMessage(LobbyPlayerLeaveMessage msg) {
        if (lobbyPlayerLeaveCallback != null) {
            lobbyPlayerLeaveCallback.onLobbyPlayerLeave(msg.getPlayerId());
        }
    }

    public void setLobbyPlayerLeaveCallback(LobbyPlayerLeaveCallback callback) {
        lobbyPlayerLeaveCallback = callback;
    }

    // Lobby leave

    public void requestLobbyLeave() {
        send(new LobbyLeaveMessage());
    }

    // Leaderboard response

    public interface LeaderboardResponseCallback {
        void onLeaderboardResponse(LeaderboardModel.Entry[] entries);
    }

    private LeaderboardResponseCallback leaderboardResponseCallback;

    private void handleLeaderboardResponse(LeaderboardResponse msg) {
        if (leaderboardResponseCallback != null) {
            leaderboardResponseCallback.onLeaderboardResponse(msg.getEntries());
        }
    }

    public void requestLeaderboard(LeaderboardResponseCallback callback) {
        leaderboardResponseCallback = callback;
        send(new LeaderboardRequest());
    }

    // Poney select begin

    public interface PoneySelectBeginCallback {
        void onPoneySelectBegin(ProtoPoney[] protoPoneys);
    }

    private PoneySelectBeginCallback poneySelectBeginCallback;

    private void handlePoneySelectBegin(PoneySelectBeginMessage msg) {
        if (poneySelectBeginCallback != null) {
            poneySelectBeginCallback.onPoneySelectBegin(msg.getProtoPoneys());
        }
    }

    public void setPoneySelectBeginCallback(PoneySelectBeginCallback callback) {
        poneySelectBeginCallback = callback;
    }

    // Poney select update

    public interface PoneySelectUpdateCallbacks {
        void onPoneySelect(long playerId, int poneyIndex);

        void onPoneyDeselect(long playerId, int poneyIndex);
    }

    private PoneySelectUpdateCallbacks poneySelectUpdateCallbacks;

    private void handlePoneySelectUpdate(PoneySelectUpdateMessage msg) {
        if (poneySelectUpdateCallbacks != null) {
            if (msg.getEvent() == PoneySelectUpdateMessage.Event.PONEY_SELECT) {
                poneySelectUpdateCallbacks.onPoneySelect(msg.getPlayerId(), msg.getPoneyIndex());
            } else if (msg.getEvent() == PoneySelectUpdateMessage.Event.PONEY_DESELECT) {
                poneySelectUpdateCallbacks.onPoneyDeselect(msg.getPlayerId(), msg.getPoneyIndex());
            }
        }
    }

    public void setPoneySelectUpdateCallback(PoneySelectUpdateCallbacks callbacks) {
        poneySelectUpdateCallbacks = callbacks;
    }

    /**
     * Requête la sélection d'un poney lors de l'écran de sélection en multi.
     * @param poneyIndex Indice du poney a sélectionner.
     */
    public void requestPoneySelect(int poneyIndex) {
        PoneySelectUpdateMessage req = new PoneySelectUpdateMessage();
        req.setEvent(PoneySelectUpdateMessage.Event.PONEY_SELECT);
        req.setPoneyIndex(poneyIndex);
        send(req);
    }

    /**
     * Requête la déselection d'un poney lors de l'écran de sélection en multi.
     * @param poneyIndex Indice du poney a déselectionner.
     */
    public void requestPoneyDeselect(int poneyIndex) {
        PoneySelectUpdateMessage req = new PoneySelectUpdateMessage();
        req.setEvent(PoneySelectUpdateMessage.Event.PONEY_DESELECT);
        req.setPoneyIndex(poneyIndex);
        send(req);
    }
}
