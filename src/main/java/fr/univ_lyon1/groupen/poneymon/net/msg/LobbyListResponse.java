package fr.univ_lyon1.groupen.poneymon.net.msg;

import fr.univ_lyon1.groupen.poneymon.model.LobbyModel;

public class LobbyListResponse implements Message {
    private LobbyModel.Description[] lobbiesDescriptions;

    public LobbyModel.Description[] getLobbiesDescriptions() {
        return lobbiesDescriptions;
    }

    public void setLobbiesDescriptions(LobbyModel.Description[] lobbiesDescriptions) {
        this.lobbiesDescriptions = lobbiesDescriptions;
    }

    @Override
    public MessageType getType() {
        return MessageType.LOBBY_LIST_RESPONSE;
    }
}
