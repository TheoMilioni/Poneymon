package fr.univ_lyon1.groupen.poneymon.controller;

import fr.univ_lyon1.groupen.poneymon.controller.ai.AIFactory;
import fr.univ_lyon1.groupen.poneymon.controller.ai.AIFactoryRegistry;
import fr.univ_lyon1.groupen.poneymon.controller.nav.EndController;
import fr.univ_lyon1.groupen.poneymon.model.FieldModel;
import fr.univ_lyon1.groupen.poneymon.model.PoneyModel;
import fr.univ_lyon1.groupen.poneymon.model.Race;
import fr.univ_lyon1.groupen.poneymon.state.State;
import fr.univ_lyon1.groupen.poneymon.state.StateDefinition;
import fr.univ_lyon1.groupen.poneymon.view.GameView;
import fr.univ_lyon1.groupen.poneymon.view.StateView;
import javafx.animation.AnimationTimer;
import javafx.event.EventHandler;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

import static javafx.scene.input.KeyCode.DOWN;
import static javafx.scene.input.KeyCode.SPACE;
import static javafx.scene.input.KeyCode.UP;


/**
 * Controleur général du jeu. Gère la boucle principale.
 */
public class GameController implements EventHandler<KeyEvent>, StateController {
    private StateOrchestrator orchestrator;
    private FieldModel fieldModel;
    private Race race;
    private GameView gameView;
    private AnimationTimer animationTimer;
    private boolean viewPaused = false;
    private PoneyModel focusedPoney;

    @SuppressWarnings("squid:S1186")
    public GameController() {}

    public void setRace(Race race) {
        this.race = race;
        setModel(new FieldModel(race));
    }

    @Override
    public void setView(StateView stateView) {
        gameView = (GameView) stateView;
        gameView.setController(this);
        gameView.setModel(fieldModel);
        startGame();
    }

    @Override
    public void setOrchestrator(StateOrchestrator orchestrator) {
        this.orchestrator = orchestrator;
    }

    class LongWrapper {
        long value;
    }

    private final LongWrapper lastNanoTime = new LongWrapper();

    /**
    * Défini le terrain de jeu pour le controller.
    * Ajout un Listener aux Poneys
    * @param fm le modèle du terrain
    */
    public void setModel(FieldModel fm) {
        fieldModel = fm;
        final PoneyModel[] poneys = fieldModel.getPoneys();
        this.focusedPoney = fieldModel.getPoneys()[0];
        for (int i = poneys.length - fieldModel.getCurrentRace().getAiControlledPoneys();
             i < poneys.length; ++i) {
            // TODO: paramétrable/random
            AIFactory factory = AIFactoryRegistry.getFactory("DodgeAI");
            poneys[i].setAI(factory.create(fieldModel, race, poneys[i], null));
        }
    }

    public PoneyModel getFocusedPoney() {
        return this.focusedPoney;
    }

    public void setFocusedPoney(int indexFocusedPoney) {
        this.focusedPoney = fieldModel.getPoneys()[indexFocusedPoney];
    }

    /*
     * Cycle de vie du jeu.
     * Calqué sur l'Activity Lifecycle d'Android.
     */

    /** Boucle principale du jeu.
     * handle() est appelee a chaque rafraichissement de frame
     * soit environ 60 fois par seconde.
     * En interne création d'un classe temporaire
     *  pour passer par valeur entre les différentes exécutions.
     */
    public void startGame() {

        animationTimer = new AnimationTimer() {
            public void handle(long currentNanoTime) {
                long deltaNano = currentNanoTime - lastNanoTime.value;
                lastNanoTime.value = currentNanoTime;
                // Deplacement, IA et affichage des poneys
                for (final PoneyModel poney : fieldModel.getPoneys()) {
                    if (poney.getAI() != null) {
                        poney.getAI().think();
                    }
                    poney.move(((double) deltaNano) / 1000000000);
                }

                if (!viewPaused) {
                    gameView.display();
                    interaction();

                    for (int j = 0; j < fieldModel.getPoneys().length; j++) {
                        for (int i = 0; i < race.getItems().size(); i++) {
                            if (race.getItems().get(i).getName().equals("end") &&
                                    race.getItems().get(i).getUsed(fieldModel.getPoneys()[j])) {
                                State end = orchestrator.createState(StateDefinition.END);
                                end.<EndController>getController().setTheWinner(
                                        fieldModel.getPoneys()[j].getColor().getName());
                                orchestrator.pushState(end);
                                animationTimer.stop();
                            }
                        }
                    }
                }
            }
        };
        resumeGame();
    }

    private void pauseGame() {
        animationTimer.stop();
    }

    private void resumeGame() {
        lastNanoTime.value = System.nanoTime();
        animationTimer.start();
    }

    public void setViewPaused(boolean paused) {
        viewPaused = paused;
    }

    @Override
    public void handle(KeyEvent event) {
        if (event.getEventType().equals(KeyEvent.KEY_PRESSED)) {
            KeyCode code;
            code = event.getCode();
            for (final PoneyModel poney : fieldModel.getPoneys()) {
                if (poney.getAI() == null) {  // TODO focus poney
                    switch (code) {
                        case UP:
                            poney.moveUp();
                            break;
                        case DOWN:
                            poney.moveDown();
                            break;
                        case SPACE:
                            poney.boost();
                            break;
                        default:
                            break;
                    }

                }
            }
        }
    }

    /**
     * Regarde les interactions entre les poneys et les items et appel les fonctions corresponsante.
     */
    private void interaction() {
        for (PoneyModel poney : fieldModel.getPoneys()) {
            for (int i = 0; i < race.getItems().size(); i++) {

                double xMinPoney = poney.getX() - 1.5;
                double xMaxPoney = xMinPoney + 0.5;
                double xMinItem = race.getItems().get(i).getX();
                double xMaxItem = race.getItems().get(i).getX() + race.getItems().get(i).getWidth();
                double yPoney = poney.getLane();
                double yItem = race.getItems().get(i).getLane();
                boolean usedItem = race.getItems().get(i).getUsed(poney);

                if ((yItem == yPoney) && (xMaxPoney > xMinItem) && (xMinPoney < xMaxItem)) {
                    race.getItems().get(i).startContact(poney);
                }
                if ((usedItem && (xMinPoney > xMaxItem))
                        || usedItem && (yItem != yPoney)) {
                    race.getItems().get(i).endContact(poney);
                }
            }
        }
    }
}
