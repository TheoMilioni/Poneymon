package fr.univ_lyon1.groupen.poneymon.controller.nav;

import fr.univ_lyon1.groupen.poneymon.controller.StateController;
import fr.univ_lyon1.groupen.poneymon.controller.StateOrchestrator;
import fr.univ_lyon1.groupen.poneymon.view.StateView;
import fr.univ_lyon1.groupen.poneymon.view.nav.MultiSetupView;

public class MultiSetupController implements StateController {
    private StateOrchestrator orchestrator;

    @Override
    public void setView(StateView stateView) {
        MultiSetupView multiSetup = (MultiSetupView) stateView;
    }

    @Override
    public void setOrchestrator(StateOrchestrator orchestrator) {
        this.orchestrator = orchestrator;
    }
}
