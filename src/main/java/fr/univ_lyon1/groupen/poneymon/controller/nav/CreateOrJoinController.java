package fr.univ_lyon1.groupen.poneymon.controller.nav;

import fr.univ_lyon1.groupen.poneymon.controller.StateController;
import fr.univ_lyon1.groupen.poneymon.controller.StateOrchestrator;
import fr.univ_lyon1.groupen.poneymon.state.State;
import fr.univ_lyon1.groupen.poneymon.state.StateDefinition;
import fr.univ_lyon1.groupen.poneymon.view.StateView;
import fr.univ_lyon1.groupen.poneymon.view.nav.CreateOrJoinView;

public class  CreateOrJoinController implements StateController {
    private StateOrchestrator orchestrator;

    @Override
    public void setView(StateView stateView) {
        CreateOrJoinView createOrJoin = (CreateOrJoinView) stateView;

        createOrJoin.getBtnBack().setOnMouseClicked(event -> orchestrator.popState());

        createOrJoin.getBtnCreate().setOnMouseClicked(event -> {
            State create = orchestrator.createState(StateDefinition.MULTICREATE);
            orchestrator.pushState(create);
        });

        createOrJoin.getBtnJoin().setOnMouseClicked(event -> {
            State join = orchestrator.createState(StateDefinition.JOIN);
            orchestrator.pushState(join);
        });
    }

    @Override
    public void setOrchestrator(StateOrchestrator orchestrator) {
        this.orchestrator = orchestrator;
    }
}
