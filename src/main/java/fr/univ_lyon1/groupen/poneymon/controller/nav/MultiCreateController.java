package fr.univ_lyon1.groupen.poneymon.controller.nav;

import fr.univ_lyon1.groupen.poneymon.controller.StateController;
import fr.univ_lyon1.groupen.poneymon.controller.StateOrchestrator;
import fr.univ_lyon1.groupen.poneymon.model.LobbyModel;
import fr.univ_lyon1.groupen.poneymon.model.ProtoPoney;
import fr.univ_lyon1.groupen.poneymon.model.Race;
import fr.univ_lyon1.groupen.poneymon.state.State;
import fr.univ_lyon1.groupen.poneymon.state.StateDefinition;
import fr.univ_lyon1.groupen.poneymon.view.StateView;
import fr.univ_lyon1.groupen.poneymon.view.nav.MultiCreateView;

import java.util.ArrayList;
import java.util.Arrays;

public class MultiCreateController implements StateController {
    private StateOrchestrator orchestrator;

    @Override
    public void setView(StateView stateView) {
        MultiCreateView view = (MultiCreateView) stateView;


        view.getBtnBack().setOnMouseClicked(event -> orchestrator.popState());

        view.getBtnCreateGame().setOnMouseClicked(event -> {
            State waitingLobby = orchestrator.createState(StateDefinition.WAITIINGLOBBY);
            LobbyModel lobby = new LobbyModel();
            lobby.getDescription().setPlayerLimit(
                    (int) view.getCbNrPlayers().getSelectionModel().getSelectedItem());
            Race race = new Race(
                (int) view.getCbFieldLength().getSelectionModel().getSelectedItem(),
                (Race.Difficulty) view.getCbDifficulty().getSelectionModel().getSelectedItem(),
                new ArrayList<>(Arrays.asList(ProtoPoney.DEFAULTS))
            );
            lobby.getDescription().setRace(race);
            waitingLobby.<WaitingLobbyController>getController().setCreateLobbyModel(lobby);
            orchestrator.pushState(waitingLobby);
        });
    }

    @Override
    public void setOrchestrator(StateOrchestrator orchestrator) {
        this.orchestrator = orchestrator;
    }
}
