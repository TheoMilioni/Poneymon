package fr.univ_lyon1.groupen.poneymon.controller.ai;

import java.util.ArrayList;
import java.util.List;

public abstract class AIFactoryRegistry {
    private static List<AIFactory> factories = new ArrayList<>();

    static {
        // Devrait être dans leur classes respectives, mais
        // https://stackoverflow.com/questions/15509337/why-doesnt-my-static-block-of-code-execute
        AIFactoryRegistry.registerAIFactory(new BourrinAI.Factory());
        AIFactoryRegistry.registerAIFactory(new LoSpeedBoosterAI.Factory());
        AIFactoryRegistry.registerAIFactory(new DodgeAI.Factory());
    }

    private AIFactoryRegistry() {
        throw new IllegalStateException("Don't instantiate AIFactoryRegistry");
    }

    static void registerAIFactory(AIFactory factory) {
        factories.add(factory);
    }

    public static List<AIFactory> getFactories() {
        return factories;
    }

    /** Obtient une Factory pour l'IA au nom donné.
     *
     * @param aiName Nom de l'IA que la Factory doit produire.
     * @return Factory de l'IA. null si non trouvée.
     */
    public static AIFactory getFactory(String aiName) {
        for (AIFactory factory : factories) {
            if (factory.getAIName().equals(aiName)) {
                return factory;
            }
        }
        return null;
    }
}
