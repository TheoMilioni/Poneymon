package fr.univ_lyon1.groupen.poneymon.controller.nav;

import fr.univ_lyon1.groupen.poneymon.controller.StateController;
import fr.univ_lyon1.groupen.poneymon.controller.StateOrchestrator;
import fr.univ_lyon1.groupen.poneymon.model.LobbyModel;
import fr.univ_lyon1.groupen.poneymon.net.Client;
import fr.univ_lyon1.groupen.poneymon.view.StateView;
import fr.univ_lyon1.groupen.poneymon.view.nav.WaitingLobbyView;
import javafx.application.Platform;

import java.util.logging.Logger;

public class WaitingLobbyController implements StateController {
    private static final Logger LOGGER = Logger.getLogger(WaitingLobbyController.class.getName());

    private StateOrchestrator orchestrator;
    private Client client = null;
    private WaitingLobbyView view;
    private LobbyModel lobby;
    private long joinLobbyID = 0;

    @Override
    public void setView(StateView stateView) {
        client = Client.getInstance();
        view = (WaitingLobbyView) stateView;

        if (client == null) {
            return;
        }

        if (joinLobbyID != 0) {
            lobby = new LobbyModel();
            lobby.getDescription().setPlayerLimit(0);
            joinLobby();
        } else {
            createLobby();
        }

        client.setLobbyPlayerLeaveCallback(playerId -> {
            lobby.getDescription().setPlayerCount(
                    lobby.getDescription().getPlayerCount() - 1);
            updateNrPlayers();
            LOGGER.info("--");
        });

        client.setLobbyPlayerJoinCallback(playerModel -> {
            lobby.getDescription().setPlayerCount(
                    lobby.getDescription().getPlayerCount() + 1);
            updateNrPlayers();
            LOGGER.info("++");
        });

        view.getBtnBack().setOnMouseClicked(event -> {
            client.setLobbyPlayerJoinCallback(null);
            client.setLobbyPlayerLeaveCallback(null);
            client.requestLobbyLeave();
            orchestrator.popState();
        });

        updateNrPlayers();
    }

    private void joinLobby() {
        client.requestLobbyJoin(joinLobbyID, new Client.LobbyJoinCallbacks() {
            @Override
            public void onLobbyJoinFailed(String message) {
                LOGGER.info("echec LobbyJoin");
            }

            @Override
            public void onLobbyJoin(LobbyModel lobbyModel) {
                LOGGER.info("ok LobbyJoin");
                lobby = lobbyModel;
                Platform.runLater(WaitingLobbyController.this::updateNrPlayers);
            }
        });
    }

    private void createLobby() {
        client.requestLobbyCreate(lobby,
                new Client.LobbyJoinCallbacks() {
            @Override
            public void onLobbyJoinFailed(String message) {
                LOGGER.info("echec LobbyCreate");
            }

            @Override
            public void onLobbyJoin(LobbyModel lobbyModel) {
                LOGGER.info("ok LobbyCreate");
                lobby = lobbyModel;
                Platform.runLater(WaitingLobbyController.this::updateNrPlayers);
            }
        });
    }

    public void setJoinLobbyID(long joinLobbyID){
        this.joinLobbyID = joinLobbyID;
    }

    public void setCreateLobbyModel(LobbyModel model) { lobby = model; }

    private void updateNrPlayers() {
        view.getTxtNrPlayer().setText(String.valueOf(lobby.getDescription().getPlayerCount()));
        view.getTxtNrMaxPlayers().setText(String.valueOf(lobby.getDescription().getPlayerLimit()));
    }

    @Override
    public void setOrchestrator(StateOrchestrator orchestrator) {
        this.orchestrator = orchestrator;
    }
}
