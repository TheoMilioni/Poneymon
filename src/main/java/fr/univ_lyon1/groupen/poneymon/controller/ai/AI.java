package fr.univ_lyon1.groupen.poneymon.controller.ai;

import fr.univ_lyon1.groupen.poneymon.model.FieldModel;
import fr.univ_lyon1.groupen.poneymon.model.PoneyModel;
import fr.univ_lyon1.groupen.poneymon.model.Race;

public abstract class AI {
    protected FieldModel field;
    protected Race race;
    protected PoneyModel poney;

    /**
     * Constructeur d'IA.
     * @param field terrain
     * @param race course
     * @param controlledPoney poney
     */
    AI(final FieldModel field, final Race race, final PoneyModel controlledPoney) {
        this.field = field;
        this.race = race;
        this.poney = controlledPoney;
    }

    /**
     * Donne le nom d'une IA.
     * @return nom
     */
    public abstract String getName();

    /**
     * Lance la réflexion et les actions d'une IA.
     */
    public abstract void think();
}
