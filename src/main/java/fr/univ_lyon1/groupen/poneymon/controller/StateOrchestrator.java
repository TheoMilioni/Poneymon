package fr.univ_lyon1.groupen.poneymon.controller;

import fr.univ_lyon1.groupen.poneymon.state.State;
import fr.univ_lyon1.groupen.poneymon.state.StateDefinition;
import fr.univ_lyon1.groupen.poneymon.view.StateFrame;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.logging.Level;
import java.util.logging.Logger;

public class StateOrchestrator {
    private static final Logger LOGGER = Logger.getLogger(StateOrchestrator.class.getName());

    private StateFrame mFrame;

    private Deque<State> stateStack = new ArrayDeque<>();

    private State currentState;

    public void setFrame(StateFrame frame) {
        mFrame = frame;
    }

    /**
     * Instancie un état selon sa définition. Permet de passer des paramètres
     * au contrôleur ou à la vue avant qu'ils ne soient utilisés.
     * @param definition Définition de l'état à instancier.
     * @return Instance de l'état.
     */
    public State createState(StateDefinition definition) {
        try {
            return new State(definition,
                    definition.getControllerClass().newInstance(),
                    definition.getViewClass().newInstance());
        } catch (InstantiationException | IllegalAccessException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }
        return null;
    }

    /**
     * Retire l'état actif et le remplace par l'état en paramètre.
     * @param state Instance de l'état à utiliser.
     */
    public void setState(State state) {
        currentState = state;
        state.getController().setOrchestrator(this);
        mFrame.setView(state.getView());
        state.getController().setView(state.getView());
    }

    public void setState(StateDefinition definition) {
        setState(createState(definition));
    }

    public void pushState(State state) {
        stateStack.push(currentState);
        setState(state);
    }

    public void popState() {
        setState(stateStack.pop());
    }

    public Deque<State> getStateStack() {
        return stateStack;
    }

    public void forgetAll() {
        stateStack.clear();
    }
}
