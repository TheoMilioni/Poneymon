package fr.univ_lyon1.groupen.poneymon.controller.nav;

import fr.univ_lyon1.groupen.poneymon.controller.StateController;
import fr.univ_lyon1.groupen.poneymon.controller.StateOrchestrator;
import fr.univ_lyon1.groupen.poneymon.model.LobbyModel;
import fr.univ_lyon1.groupen.poneymon.net.Client;
import fr.univ_lyon1.groupen.poneymon.state.State;
import fr.univ_lyon1.groupen.poneymon.state.StateDefinition;
import fr.univ_lyon1.groupen.poneymon.view.StateView;
import fr.univ_lyon1.groupen.poneymon.view.nav.JoinView;
import javafx.application.Platform;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

import java.net.URISyntaxException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class JoinController implements StateController {
    private static final Logger LOGGER = Logger.getLogger(JoinController.class.getName());

    private StateOrchestrator orchestrator;
    private JoinView view;

    public class Game {
        private Long id;
        private SimpleStringProperty colNickname;
        private SimpleIntegerProperty colNrPlayers;
        private SimpleIntegerProperty colNrPlayersMax;
        private SimpleIntegerProperty colLength;
        private SimpleStringProperty colDifficulty;
        private SimpleStringProperty colRanked;

        public Game(Long id, SimpleStringProperty colNickname, SimpleIntegerProperty colNrPlayers, SimpleIntegerProperty colNrPlayersMax, SimpleIntegerProperty colLength, SimpleStringProperty colDifficulty, SimpleStringProperty colRanked) {
            this.id = id;
            this.colNickname = colNickname;
            this.colNrPlayers = colNrPlayers;
            this.colNrPlayersMax = colNrPlayersMax;
            this.colLength = colLength;
            this.colDifficulty = colDifficulty;
            this.colRanked = colRanked;
        }

        public Long getId() {
            return id;
        }

        public String getColNickname() {
            return colNickname.get();
        }

        public SimpleStringProperty colNicknameProperty() {
            return colNickname;
        }

        public int getColNrPlayers() {
            return colNrPlayers.get();
        }

        public SimpleIntegerProperty colNrPlayersProperty() {
            return colNrPlayers;
        }

        public int getColNrPlayersMax() {
            return colNrPlayersMax.get();
        }

        public SimpleIntegerProperty colNrPlayersMaxProperty() {
            return colNrPlayersMax;
        }

        public int getColLength() {
            return colLength.get();
        }

        public SimpleIntegerProperty colLengthProperty() {
            return colLength;
        }

        public String getColDifficulty() {
            return colDifficulty.get();
        }

        public SimpleStringProperty colDifficultyProperty() {
            return colDifficulty;
        }

        public String getColRanked() {
            return colRanked.get();
        }

        public SimpleStringProperty colRankedProperty() {
            return colRanked;
        }
    }

    @Override
    public void setView(StateView stateView) {
        view = (JoinView) stateView;
        view.getBtnBack().setOnMouseClicked(event -> orchestrator.popState());

        view.getBtnRefresh().setOnMouseClicked(event -> fillTable(createList()));

        view.getTvCurrentGames().setOnMouseClicked(event -> view.getBtnValidate().setDisable(
                ((TableView) event.getSource()).getSelectionModel().isEmpty()));

        view.getBtnValidate().setOnMouseClicked(event -> {
            State waitingLobby = orchestrator.createState(StateDefinition.WAITIINGLOBBY);

            final Game game = (Game) view.getTvCurrentGames().getSelectionModel().getSelectedItem();
            waitingLobby.<WaitingLobbyController>getController().setJoinLobbyID(game.id);

            orchestrator.pushState(waitingLobby);
        });

        fillTable(createList());

    }

    private ObservableList<Game> createList() {
        Client client = Client.getInstance();
        ObservableList<Game> gamesList = FXCollections.observableArrayList();
        if (client == null) {
            return gamesList;
        }
        client.requestLobbyList(entries -> {
            for (LobbyModel.Description description : entries) {
                gamesList.add(new Game(description.getLobbyId(),
                        new SimpleStringProperty(description.getName()+""),
                        new SimpleIntegerProperty(description.getPlayerCount()),
                        new SimpleIntegerProperty(description.getPlayerLimit()),
                        new SimpleIntegerProperty((int) description.getRace().getRaceLength()),
                        new SimpleStringProperty(description.getRace().getDifficulty().getName()),
                        new SimpleStringProperty(description.isRanked() ? "Oui" : "Non")));
            }
            Platform.runLater(() -> view.getTvCurrentGames().refresh());
        });
        return gamesList;
    }

    private void fillTable(ObservableList list) {
        view.getColNickname().setCellValueFactory(
                new PropertyValueFactory<JoinController.Game, String>("colNickname"));
        view.getColNrPlayers().setCellValueFactory(
                new PropertyValueFactory<JoinController.Game, Integer>("colNrPlayers"));
        view.getColLength().setCellValueFactory(
                new PropertyValueFactory<JoinController.Game, Integer>("colLength"));
        view.getColDifficulty().setCellValueFactory(
                new PropertyValueFactory<JoinController.Game, String>("colDifficulty"));
        view.getColRanked().setCellValueFactory(
                new PropertyValueFactory<JoinController.Game, String>("colRanked"));
        view.getColNrPlayersMax().setCellValueFactory(
                new PropertyValueFactory<JoinController.Game, Integer>("colNrPlayersMax"));
        view.getTvCurrentGames().setItems(list);
    }

    @Override
    public void setOrchestrator(StateOrchestrator orchestrator) {
        this.orchestrator = orchestrator;
    }
}
