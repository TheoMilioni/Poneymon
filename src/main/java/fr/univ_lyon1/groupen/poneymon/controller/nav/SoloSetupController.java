package fr.univ_lyon1.groupen.poneymon.controller.nav;

import fr.univ_lyon1.groupen.poneymon.controller.GameController;
import fr.univ_lyon1.groupen.poneymon.controller.StateController;
import fr.univ_lyon1.groupen.poneymon.controller.StateOrchestrator;
import fr.univ_lyon1.groupen.poneymon.model.ProtoPoney;
import fr.univ_lyon1.groupen.poneymon.model.Race;
import fr.univ_lyon1.groupen.poneymon.state.State;
import fr.univ_lyon1.groupen.poneymon.state.StateDefinition;
import fr.univ_lyon1.groupen.poneymon.view.StateView;
import fr.univ_lyon1.groupen.poneymon.view.nav.SoloSetupView;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SoloSetupController implements StateController {
    private static final Logger LOGGER = Logger.getLogger(SoloSetupController.class.getName());

    private StateOrchestrator orchestrator;
    private ProtoPoney selectedProtoPoney = ProtoPoney.DEFAULTS[0];

    @Override
    public void setView(StateView stateView) {
        final SoloSetupView view = (SoloSetupView) stateView;

        view.getButtonValidate().setOnAction(event -> {
            State game = orchestrator.createState(StateDefinition.GAME);

            List<ProtoPoney> protoPoneyList = new ArrayList<>();
            protoPoneyList.add(selectedProtoPoney);

            List<ProtoPoney> protoPoneyListDefault = new ArrayList<>();
            for (int i = 0; i < 5; i++) {
                if (ProtoPoney.DEFAULTS[i].getColor() != selectedProtoPoney.getColor()) {
                    protoPoneyListDefault.add(ProtoPoney.DEFAULTS[i]);
                }
            }
            protoPoneyList.add(protoPoneyListDefault.get(0));
            protoPoneyList.add(protoPoneyListDefault.get(1));
            protoPoneyList.add(protoPoneyListDefault.get(2));

            Race race = new Race((int) view.getCbFieldLength().getValue(),
                    (Race.Difficulty) view.getCbDifficulty().getValue(),
                    protoPoneyList);
            race.setAiControlledPoneys(3);
            race.generateMotif();

            game.<GameController>getController().setRace(race);
            game.<GameController>getController().setFocusedPoney(0);
            orchestrator.pushState(game);
        });

        view.getBtnBack().setOnMouseClicked(event -> orchestrator.popState());

        // On génère les 5 cartes de protoponeys
        for (int i = 0; i < view.getGridpaneListeProtoPoneys().getColumnConstraints().size(); i++) {
            ProtoPoney currentProtoPoney = ProtoPoney.DEFAULTS[i];
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/fxml/ProtoPoney.fxml"));
            VBox currentCard = null;
            try {
                currentCard = loader.load();
            } catch (IOException e) {
                LOGGER.log(Level.SEVERE, e.getMessage(), e);
            }
            currentCard.setUserData(currentProtoPoney);

            final String PONYPREFIX = "/assets/pony-";
            final Image poneyStillImage = new Image(getClass().getResource(
                    PONYPREFIX + currentProtoPoney.getColor().getName() + ".png").toExternalForm());
            final Image poneyRunningImage = new Image(getClass().getResource(
                    PONYPREFIX + currentProtoPoney.getColor().getName() +
                            "-running-minimal.gif").toExternalForm());

            // Défini le comportement au clique sur les cartes
            currentCard.setOnMouseClicked(event -> {
                GridPane clickedGridPane = view.getGridpaneListeProtoPoneys();
                for (Node n : clickedGridPane.getChildren()) {
                    ProtoPoney cardProtoPoney = (ProtoPoney) n.getUserData();
                    n.setStyle("-fx-border-style: hidden");
                    ((ImageView)((Parent)n).getChildrenUnmodifiable().get(0)).setImage(new Image(
                            getClass().getResource(PONYPREFIX +
                                    cardProtoPoney.getColor().getName() + ".png").toExternalForm()));
                }

                VBox clickedCard = (VBox)event.getSource();
                selectedProtoPoney = currentProtoPoney;
                ((ImageView)clickedCard.getChildren().get(0)).setImage(poneyRunningImage);
                clickedCard.setStyle("-fx-border-style: solid;" +
                        "-fx-border-width: 3;" +
                        "-fx-border-insets: 5;" +
                        "-fx-border-radius: 8;" +
                        "-fx-border-color: grey;");
                LOGGER.info(selectedProtoPoney.getColor().getName());
            });

            ((ImageView)loader.getNamespace().get("image_poney")).setImage(poneyStillImage);
            ((Text)loader.getNamespace().get("vitesse")).setText(currentProtoPoney.getSpeed()+"");
            ((Text)loader.getNamespace().get("acceleration")).setText(currentProtoPoney.getAcceleration()+"");
            ((Text)loader.getNamespace().get("boost")).setText(currentProtoPoney.getBoostSpeed()+"");
            view.getGridpaneListeProtoPoneys().add(currentCard, i, 0);
        }
    }

    @Override
    public void setOrchestrator(StateOrchestrator orchestrator) {
        this.orchestrator = orchestrator;
    }
}
