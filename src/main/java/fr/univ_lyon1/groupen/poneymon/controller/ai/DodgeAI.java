package fr.univ_lyon1.groupen.poneymon.controller.ai;

import fr.univ_lyon1.groupen.poneymon.model.FieldModel;
import fr.univ_lyon1.groupen.poneymon.model.PoneyModel;
import fr.univ_lyon1.groupen.poneymon.model.Race;

import java.util.Map;
import java.util.Random;

/** LoSpeedBooster, une IA qui boost quand le poney va lentement.
 * Si le poney va a moins de la moitié de la vitesse maxi, booste le poney.
 * Si le poney n'a pas boosté et qu'il en est à son dernier tour, booste pour la fin.
 */
public class DodgeAI extends AI {

    private static final String NAME = "DodgeAI";

    public static class Factory implements AIFactory {
        @Override
        public String getAIName() {
            return NAME;
        }

        @Override
        public AI create(FieldModel field,
                         Race race,
                         PoneyModel controlledPoney,
                         Map<String, Object> params) {
            return new DodgeAI(field, race, controlledPoney);
        }
    }

    private final Random random = new Random();

    private DodgeAI(FieldModel field, Race race, PoneyModel controlledPoney) {
        super(field, race, controlledPoney);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void think() {
        poney.boost();
        for (int i = 0; i < race.getItems().size(); i++) {
            if (race.getItems().get(i).getName().equals("wall")
                    || race.getItems().get(i).getName().equals("mud")) {

                double xPoney = poney.getX();
                double xMinItem = race.getItems().get(i).getX();
                double xMaxItem = race.getItems().get(i).getX() + race.getItems().get(i).getWidth();

                double yPoney = poney.getLane();
                double yItem = race.getItems().get(i).getLane();
                double anticipation = 1;

                if (yItem == yPoney &&
                        (xPoney + anticipation > xMinItem) &&
                        (xPoney < xMaxItem)) {
                    int action = random.nextInt(2);
                    if (yPoney == 0) {
                        if (action == 0) {
                            poney.moveDown();
                        }
                        if (action == 1) {
                            poney.moveDown();
                            poney.moveDown();
                        }
                    }
                    if (yPoney == 1) {
                        if (action == 0) {
                            poney.moveUp();
                        }
                        if (action == 1) {
                            poney.moveDown();
                        }
                    }
                    if (yPoney == 2) {
                        if (action == 0) {
                            poney.moveUp();
                        }
                        if (action == 1) {
                            poney.moveUp();
                            poney.moveUp();
                        }
                    }
                }
            }
        }
    }
}
