package fr.univ_lyon1.groupen.poneymon.controller.nav;

import fr.univ_lyon1.groupen.poneymon.controller.StateController;
import fr.univ_lyon1.groupen.poneymon.controller.StateOrchestrator;
import fr.univ_lyon1.groupen.poneymon.model.LeaderboardModel;
import fr.univ_lyon1.groupen.poneymon.net.Client;
import fr.univ_lyon1.groupen.poneymon.view.StateView;
import fr.univ_lyon1.groupen.poneymon.view.nav.LeaderboardView;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.cell.PropertyValueFactory;

import java.net.URISyntaxException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class LeaderboardController implements StateController {
    private static final Logger LOGGER = Logger.getLogger(LeaderboardController.class.getName());

    private StateOrchestrator orchestrator;
    private LeaderboardView view;

    public class UserScore {
        private SimpleStringProperty tableColumnPseudo;
        private SimpleStringProperty tableColumnScore;

        public UserScore(SimpleStringProperty pseudo, SimpleStringProperty score) {
            this.tableColumnPseudo = pseudo;
            this.tableColumnScore = score;
        }

        public String getTableColumnPseudo() {
            return tableColumnPseudo.get();
        }

        public SimpleStringProperty tableColumnPseudoProperty() {
            return tableColumnPseudo;
        }

        public String getTableColumnScore() {
            return tableColumnScore.get();
        }

        public SimpleStringProperty tableColumnScoreProperty() {
            return tableColumnScore;
        }
    }

    @Override
    public void setView(StateView stateView) {

        view = (LeaderboardView) stateView;

        view.getBtnBack().setOnMouseClicked(event -> orchestrator.popState());
        
        ObservableList<UserScore> list = createList();
        fillTable(list);
    }

    private ObservableList<UserScore> createList(){
        Client client = Client.getInstance();
        ObservableList<UserScore> finalList = FXCollections.observableArrayList();
        if (client == null) {
            return finalList;
        }
        client.requestLeaderboard(entries -> {
            for (LeaderboardModel.Entry entry : entries) {
                finalList.add(new UserScore(new SimpleStringProperty(entry.getNickname()),
                        new SimpleStringProperty(entry.getScore()+"")));
            }
        });
        return finalList;
    }

    private void fillTable(ObservableList list) {
        view.getTcNickname().setCellValueFactory(
                new PropertyValueFactory<UserScore, String>("tableColumnPseudo"));
        view.getTcScore().setCellValueFactory(
                new PropertyValueFactory<UserScore, String>("tableColumnScore"));
        view.getTvLeaderboard().setItems(list);
    }

    @Override
    public void setOrchestrator(StateOrchestrator orchestrator) {
        this.orchestrator = orchestrator;
    }

}
