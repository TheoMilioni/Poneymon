package fr.univ_lyon1.groupen.poneymon.controller.nav;

import fr.univ_lyon1.groupen.poneymon.controller.StateController;
import fr.univ_lyon1.groupen.poneymon.controller.StateOrchestrator;
import fr.univ_lyon1.groupen.poneymon.state.State;
import fr.univ_lyon1.groupen.poneymon.state.StateDefinition;
import fr.univ_lyon1.groupen.poneymon.view.StateView;
import fr.univ_lyon1.groupen.poneymon.view.nav.MainMenuView;

public class MainMenuController implements StateController {
    private StateOrchestrator orchestrator;

    @Override
    public void setView(StateView stateView) {
        MainMenuView mainMenu = (MainMenuView) stateView;
        
        mainMenu.getBtnSingleplayer().setOnAction(event -> {
            State  soloSetup = orchestrator.createState(StateDefinition.SOLOSETUP);
            orchestrator.pushState(soloSetup);
        });
        
        mainMenu.getBtnMultiplayer().setOnAction(event -> {
            State multiconnection = orchestrator.createState(StateDefinition.MULTICONNECTION);
            orchestrator.pushState(multiconnection);
        });
        
        mainMenu.getBtnLeaderboard().setOnAction(event -> {
            State leaderboard = orchestrator.createState(StateDefinition.LEADERBOARD);
            orchestrator.pushState(leaderboard);
        });
        
        mainMenu.getBtnExit().setOnAction(event -> System.exit(0));
    }

    @Override
    public void setOrchestrator(StateOrchestrator orchestrator) {
        this.orchestrator = orchestrator;
    }
}
