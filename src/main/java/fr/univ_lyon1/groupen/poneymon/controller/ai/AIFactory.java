package fr.univ_lyon1.groupen.poneymon.controller.ai;

import fr.univ_lyon1.groupen.poneymon.model.FieldModel;
import fr.univ_lyon1.groupen.poneymon.model.PoneyModel;
import fr.univ_lyon1.groupen.poneymon.model.Race;

import java.util.Map;

public interface AIFactory {
    String getAIName();

    AI create(final FieldModel field, final Race race,
              final PoneyModel controlledPoney,
              Map<String, Object> params);
}
