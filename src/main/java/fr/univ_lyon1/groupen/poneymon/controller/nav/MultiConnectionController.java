package fr.univ_lyon1.groupen.poneymon.controller.nav;

import fr.univ_lyon1.groupen.poneymon.controller.StateController;
import fr.univ_lyon1.groupen.poneymon.controller.StateOrchestrator;
import fr.univ_lyon1.groupen.poneymon.net.Client;
import fr.univ_lyon1.groupen.poneymon.state.State;
import fr.univ_lyon1.groupen.poneymon.state.StateDefinition;
import fr.univ_lyon1.groupen.poneymon.view.StateView;
import fr.univ_lyon1.groupen.poneymon.view.nav.MultiConnectionView;
import javafx.application.Platform;
import javafx.scene.paint.Color;

import java.net.URISyntaxException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MultiConnectionController implements StateController {
    private static final Logger LOGGER = Logger.getLogger(MultiConnectionController.class.getName());

    private StateOrchestrator orchestrator;
    private String pseudo;
    private String password;

    @Override
    public void setView(StateView stateView) {
        MultiConnectionView view = (MultiConnectionView) stateView;

        view.getBtnBack().setOnMouseClicked(event -> orchestrator.popState());

        view.getPseudoField().setOnInputMethodTextChanged(event ->
                view.getBtnSignin().setDisable(
                        view.getPseudo().equals("") || view.getPassword().equals("")));

        view.getPasswordField().setOnInputMethodTextChanged(event ->
                view.getBtnSignin().setDisable(
                        view.getPseudo().equals("") || view.getPassword().equals("")));

        view.getBtnSignin().setOnAction(event -> {
            password = view.getPassword();
            pseudo = view.getPseudo();
            Client client = Client.getInstance();
            if (client == null) {
                return;
            }
            client.requestLogin(new Client.LoginCallbacks() {
                @Override
                public void onLoginSuccess(long playerId, String token) {
                    view.getTxtInfos().setText("Connexion réussie");
                    view.getTxtInfos().setFill(Color.GREENYELLOW);
                    Platform.runLater(() -> {
                        State createOrJoin = orchestrator.createState(StateDefinition.CREATEORJOIN);
                        orchestrator.pushState(createOrJoin);
                    });
                }

                @Override
                public void onLoginFail(String message) {
                    view.getTxtInfos().setText("Echec de la connexion");
                    view.getTxtInfos().setFill(Color.RED);
                }
            }, pseudo, password, false);

        });

        view.getBtnSignup().setOnAction(event -> {
            password = view.getPassword();
            pseudo = view.getPseudo();
            Client client = Client.getInstance();
            if (client == null) {
                return;
            }
            client.requestLogin(new Client.LoginCallbacks() {
                @Override
                public void onLoginSuccess(long playerId, String token) {
                    view.getTxtInfos().setText("Inscription réussie");
                    view.getTxtInfos().setFill(Color.GREENYELLOW);
                }

                @Override
                public void onLoginFail(String message) {
                    view.getTxtInfos().setText("Echec de l'inscription");
                    view.getTxtInfos().setFill(Color.RED);
                }
            }, pseudo, password, true);
        });

    }

    @Override
    public void setOrchestrator(StateOrchestrator orchestrator) {
        this.orchestrator = orchestrator;
    }
}
