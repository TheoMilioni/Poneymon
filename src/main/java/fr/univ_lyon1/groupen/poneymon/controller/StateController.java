package fr.univ_lyon1.groupen.poneymon.controller;

import fr.univ_lyon1.groupen.poneymon.view.StateView;

public interface StateController {
    /**
     * Indique au contrôleur quelle est la vue.
     * @param stateView StateView respective de l'état.
     */
    void setView(StateView stateView);

    /**
     * Passe l'instance de l'orchestrateur d'état au contrôleur.
     * @param orchestrator Instance de l'orchestrateur.
     */
    void setOrchestrator(StateOrchestrator orchestrator);
}
