package fr.univ_lyon1.groupen.poneymon.controller.ai;

import fr.univ_lyon1.groupen.poneymon.model.FieldModel;
import fr.univ_lyon1.groupen.poneymon.model.PoneyModel;
import fr.univ_lyon1.groupen.poneymon.model.Race;

import java.util.Map;

/** LoSpeedBooster, une IA qui boost quand le poney va lentement.
 * Si le poney va a moins de la moitié de la vitesse maxi, booste le poney.
 * Si le poney n'a pas boosté et qu'il en est à son dernier tour, booste pour la fin.
 */
public class LoSpeedBoosterAI extends AI {

    private static final String NAME = "LoSpeedBoosterAI";

    public static class Factory implements AIFactory {
        @Override
        public String getAIName() {
            return NAME;
        }

        @Override
        public AI create(FieldModel field,
                         Race race,
                         PoneyModel controlledPoney,
                         Map<String, Object> params) {
            return new LoSpeedBoosterAI(field, race, controlledPoney);
        }
    }

    private LoSpeedBoosterAI(FieldModel field, Race race, PoneyModel controlledPoney) {
        super(field, race, controlledPoney);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void think() {
        if (poney.isBoostAvailable()
                && poney.getSpeed() < PoneyModel.MIN_SPEED + PoneyModel.MAX_SPEED / 2
                || poney.getCurrentRace() != null) {
            poney.boost();
        }
    }

}
