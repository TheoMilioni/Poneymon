package fr.univ_lyon1.groupen.poneymon.controller.nav;

import fr.univ_lyon1.groupen.poneymon.controller.StateController;
import fr.univ_lyon1.groupen.poneymon.controller.StateOrchestrator;
import fr.univ_lyon1.groupen.poneymon.state.State;
import fr.univ_lyon1.groupen.poneymon.state.StateDefinition;
import fr.univ_lyon1.groupen.poneymon.view.StateView;
import fr.univ_lyon1.groupen.poneymon.view.nav.EndView;

public class EndController implements StateController {
    private StateOrchestrator orchestrator;
    private EndView end;
    private String winner;

    @Override
    public void setView(StateView stateView) {
        end = (EndView) stateView;

        end.getBtnMainMenu().setOnMouseClicked(event -> {
            State mainmenu = orchestrator.createState(StateDefinition.MAIN_MENU);
            orchestrator.forgetAll();
            orchestrator.setState(mainmenu);
        });

        setWinner();
    }

    public void setTheWinner(String winner) {
        this.winner = winner;
    }

    public void setWinner() {
        end.getTxtWinner().setText(winner);
    }

    @Override
    public void setOrchestrator(StateOrchestrator orchestrator) {
        this.orchestrator = orchestrator;
    }
}
