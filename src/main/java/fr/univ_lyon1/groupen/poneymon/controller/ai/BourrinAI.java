package fr.univ_lyon1.groupen.poneymon.controller.ai;

import fr.univ_lyon1.groupen.poneymon.model.FieldModel;
import fr.univ_lyon1.groupen.poneymon.model.PoneyModel;
import fr.univ_lyon1.groupen.poneymon.model.Race;

import java.util.Map;
import java.util.Random;

/** BourrinAI, l'IA qui bourrine.
 * Booste le poney dès qu'il peut.
 */
public class BourrinAI extends AI {

    private static final String NAME = "BourrinAI";

    public static class Factory implements AIFactory {
        @Override
        public String getAIName() {
            return NAME;
        }

        @Override
        public AI create(FieldModel field, Race race,
                         PoneyModel controlledPoney,
                         Map<String, Object> params) {
            return new BourrinAI(field, race, controlledPoney);
        }
    }

    private final Random random = new Random();

    private BourrinAI(FieldModel field, Race race, PoneyModel controlledPoney) {
        super(field, race, controlledPoney);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void think() {
        poney.boost();
        int action = random.nextInt(1000);  // 0 à 999
        if (action > 998) {
            poney.moveDown();
        }
        if (action < 1) {
            poney.moveUp();
        }
    }
}
