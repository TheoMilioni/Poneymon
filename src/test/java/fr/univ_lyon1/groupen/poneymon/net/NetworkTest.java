package fr.univ_lyon1.groupen.poneymon.net;

import com.esotericsoftware.kryo.io.Output;
import fr.univ_lyon1.groupen.poneymon.net.msg.DebugMessage;
import fr.univ_lyon1.groupen.poneymon.net.msg.Message;
import org.junit.Test;

import java.nio.ByteBuffer;

import static org.junit.Assert.*;

public class NetworkTest {
    @Test
    public void testParseMessage() {
        Output messageOutput = new Output(1024, -1);
        DebugMessage msg = new DebugMessage();
        msg.setDebugText("hello");
        messageOutput.write(new byte[] { (byte) msg.getType().getTypeID(), 0, 0, 0 });
        Kryotik.getKryo().writeObject(messageOutput, msg);
        ByteBuffer bbuf = ByteBuffer.wrap(messageOutput.toBytes());
        Message decodedMsg = Network.parseMessage(bbuf);
        assertTrue(decodedMsg instanceof DebugMessage);
        assertEquals("hello", ((DebugMessage) decodedMsg).getDebugText());
    }

    @Test
    public void testParseMessageNull() {
        ByteBuffer bbuf = ByteBuffer.wrap(new byte[] { 0, 0, 0, 0 });
        Message decodedMsg = Network.parseMessage(bbuf);
        assertNull(decodedMsg);
    }
}
