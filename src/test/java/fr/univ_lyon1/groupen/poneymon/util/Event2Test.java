package fr.univ_lyon1.groupen.poneymon.util;

import org.junit.Test;

import static org.junit.Assert.*;

public class Event2Test {
    class PrimitiveEventTester {
        Event2<PrimitiveEventTester, Integer, String> onEvent = new Event2<>(this);
    }
    private boolean petFired;

    @Test
    public void testPrimitiveEventSubscription() {
        PrimitiveEventTester pet = new PrimitiveEventTester();
        petFired = false;
        pet.onEvent.subscribe((sourceObject, i, s) -> {
            assertEquals(1337, (int) i);
            assertEquals("testy", s);
            petFired = true;
        });
        pet.onEvent.fire(1337, "testy");
        assertTrue(petFired);
    }

    @Test
    public void testPrimitiveEventUnsubscription() {
        PrimitiveEventTester pet = new PrimitiveEventTester();
        petFired = false;
        Event2.Callback<PrimitiveEventTester, Integer, String> callback = (sourceObject, i, s) -> petFired = true;
        pet.onEvent.subscribe(callback);
        pet.onEvent.fire(555, "zubscribe");
        assertTrue(petFired);
        petFired = false;
        pet.onEvent.unsuscribe(callback);
        pet.onEvent.fire(777, "goodbye");
        assertFalse(petFired);
    }
}