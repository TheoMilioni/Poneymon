package fr.univ_lyon1.groupen.poneymon.util;

import org.junit.Test;

import static org.junit.Assert.*;

public class Event1Test {
    class PrimitiveEventTester {
        Event1<PrimitiveEventTester, Integer> onEvent = new Event1<>(this);
    }
    private boolean petFired;

    @Test
    public void testPrimitiveEventSubscription() {
        PrimitiveEventTester pet = new PrimitiveEventTester();
        petFired = false;
        pet.onEvent.subscribe((sourceObject, eventData) -> {
            assertEquals(42, (int) eventData);
            petFired = true;
        });
        pet.onEvent.fire(42);
        assertTrue(petFired);
    }

    @Test
    public void testPrimitiveEventUnsubscription() {
        PrimitiveEventTester pet = new PrimitiveEventTester();
        petFired = false;
        Event1.Callback<PrimitiveEventTester, Integer> callback = (sourceObject, eventData) -> {
            assertEquals(42, (int) eventData);
            petFired = true;
        };
        pet.onEvent.subscribe(callback);
        pet.onEvent.fire(42);
        assertTrue(petFired);
        petFired = false;
        pet.onEvent.unsuscribe(callback);
        pet.onEvent.fire(42);
        assertFalse(petFired);
    }

    class AnObject {
        int a;
        boolean b;
        String c;
    }
    class AnObjectEventTester {
        Event1<AnObjectEventTester, AnObject> onEvent = new Event1<>(this);
    }
    private boolean aoetFired;

    @Test
    public void testAnObjectEvent() {
        AnObjectEventTester aoet = new AnObjectEventTester();
        aoetFired = false;
        AnObject ao = new AnObject();
        ao.a = 8;
        ao.b = true;
        ao.c = "hello";
        aoet.onEvent.subscribe((sourceObject, eventData) -> {
            assertEquals(8, eventData.a);
            assertTrue(eventData.b);
            assertEquals("hello", eventData.c);
            aoetFired = true;
        });
        aoet.onEvent.fire(ao);
        assertTrue(aoetFired);
    }

    class OpaqueObjectEventTester {
        Event1<OpaqueObjectEventTester, Object> onEvent = new Event1<>(this);
    }
    private boolean ooetFired;

    @Test
    public void testOpaqueObjectEvent() {
        OpaqueObjectEventTester ooet = new OpaqueObjectEventTester();
        ooetFired = false;
        AnObject ao = new AnObject();
        ao.a = 88;
        ao.b = false;
        ao.c = "hello2";
        ooet.onEvent.subscribe((sourceObject, eventData) -> {
            assertNotNull(eventData);
            assertTrue(eventData instanceof AnObject);
            AnObject ao2 = (AnObject) eventData;
            assertEquals(88, ao2.a);
            assertFalse(ao2.b);
            assertEquals("hello2", ao2.c);
            ooetFired = true;
        });
        ooet.onEvent.fire(ao);
        assertTrue(ooetFired);
    }
}
