package fr.univ_lyon1.groupen.poneymon.util;

import org.junit.Test;

import static org.junit.Assert.*;

public class EventTest {
    class PrimitiveEventTester {
        Event<PrimitiveEventTester> onEvent = new Event<>(this);
    }
    private boolean petFired;

    @Test
    public void testPrimitiveEventSubscription() {
        PrimitiveEventTester pet = new PrimitiveEventTester();
        petFired = false;
        pet.onEvent.subscribe((sourceObject) -> petFired = true);
        pet.onEvent.fire();
        assertTrue(petFired);
    }

    @Test
    public void testPrimitiveEventUnsubscription() {
        PrimitiveEventTester pet = new PrimitiveEventTester();
        petFired = false;
        Event.Callback<PrimitiveEventTester> callback = (sourceObject) -> petFired = true;
        pet.onEvent.subscribe(callback);
        pet.onEvent.fire();
        assertTrue(petFired);
        petFired = false;
        pet.onEvent.unsuscribe(callback);
        pet.onEvent.fire();
        assertFalse(petFired);
    }
}