package fr.univ_lyon1.groupen.poneymon.model;

import fr.univ_lyon1.groupen.poneymon.controller.GameController;
import fr.univ_lyon1.groupen.poneymon.model.item.*;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ItemModelTest {
    @Test
    public void creationTest() {
        Item item1 = new MudModel(100,0,3);
        Item item2 = new WallModel(200,1,1);
        Item item3 = new EndModel(300,2,2);
        Item item4 = new IceModel(100,0,5);
        Item item5 = new BonusModel(200,1,1);

        assertEquals("mud", item1.getName());
        assertEquals("wall", item2.getName());
        assertEquals("end", item3.getName());
        assertEquals("ice", item4.getName());
        assertEquals("bonus", item5.getName());
        //Test des getters
        assert(100 == item1.getX()); assert(0 == item1.getLane()); assert(3 == item1.getWidth());
        assert(200 == item2.getX()); assert(1 == item2.getLane()); assert(1 == item2.getWidth());
        assert(300 == item3.getX()); assert(2 == item3.getLane()); assert(2 == item3.getWidth());
        assert(100 == item4.getX()); assert(0 == item4.getLane()); assert(5 == item4.getWidth());
        assert(200 == item5.getX()); assert(1 == item5.getLane()); assert(1 == item5.getWidth());
        //Test des setters
        // pas de setters
    }

    @Test
    public void mudEffect(){
        ProtoPoney protoPoney = new ProtoPoney();
        PoneyModel poneyModel = new PoneyModel(protoPoney,0);

        Item mudModel = new MudModel(100,0,3);

        poneyModel.move(1);
        mudModel.startContact(poneyModel);
        poneyModel.move(1);
        assert (poneyModel.getMaxSpeed() == 1);
        assert (poneyModel.isNerf());
        mudModel.endContact(poneyModel);
        poneyModel.move(1);
        assert (!(poneyModel.getMaxSpeed() == 1));
        assert (!poneyModel.isNerf());

        //Test : boue utilisée et visible
        assert (mudModel.getUsed(poneyModel));
        assert (mudModel.isObservable(poneyModel));
    }

    @Test
    public void wallEffect(){
        ProtoPoney protoPoney = new ProtoPoney();
        PoneyModel poneyModel = new PoneyModel(protoPoney,0);

        Item wallModel = new WallModel(100,0,1);

        //Test : contact avec un mur
        poneyModel.move(10);
        assert (poneyModel.getSpeed() > 1);
        wallModel.startContact(poneyModel);
        poneyModel.move(0);
        assert (poneyModel.getSpeed() < 0.01);
        wallModel.endContact(poneyModel);
        poneyModel.move(10);
        assert (poneyModel.getSpeed() > 1);

        //Test : mur utilisé et non visible
        assert (wallModel.getUsed(poneyModel));
        assert (!wallModel.isObservable(poneyModel));

        //Test : contact avec le MEME mur
        poneyModel.move(10);
        wallModel.startContact(poneyModel);
        poneyModel.move(0);
        assert (poneyModel.getSpeed() > 1);
        wallModel.endContact(poneyModel);
        poneyModel.move(0);
        assert (poneyModel.getSpeed() > 1);
    }

    @Test
    public void endEffect(){
        ProtoPoney protoPoney = new ProtoPoney();
        PoneyModel poneyModel = new PoneyModel(protoPoney,0);

        Item endModel = new EndModel(100,0,1);

        poneyModel.move(1);
        endModel.startContact(poneyModel);
        poneyModel.move(1);
        endModel.endContact(poneyModel);
        poneyModel.move(1);

        //Test : la fin est pas utilisé et visible
        assert (!endModel.getUsed(poneyModel));
        assert (endModel.isObservable(poneyModel));
    }

    @Test
    public void iceEffect(){
        ProtoPoney protoPoney = new ProtoPoney();
        PoneyModel poneyModel = new PoneyModel(protoPoney,0);

        Item iceModel = new IceModel(100,0,1);

        poneyModel.move(5);
        double speedBefore = poneyModel.getSpeed();
        int laneBefore =  poneyModel.getLane();
        iceModel.startContact(poneyModel);
        poneyModel.move(1);
        assert (poneyModel.getSpeed() == speedBefore);
        assert (poneyModel.isFreeeze());
        poneyModel.moveDown();
        assert (poneyModel.getLane() == laneBefore);
        poneyModel.moveUp();
        assert (poneyModel.getLane() == laneBefore);
        iceModel.endContact(poneyModel);
        poneyModel.move(1);
        assert (!(poneyModel.getSpeed() == speedBefore));
        assert (!poneyModel.isFreeeze());

        //Test : glace utilisé et visible
        assert (iceModel.getUsed(poneyModel));
        assert (iceModel.isObservable(poneyModel));
    }

    @Test
    public void bonusEffect(){
        ProtoPoney protoPoney = new ProtoPoney();
        PoneyModel poneyModel = new PoneyModel(protoPoney,0);

        Item bonusModel = new BonusModel(100,0,1);

        poneyModel.move(1);
        bonusModel.startContact(poneyModel);
        poneyModel.move(1);
        assert (poneyModel.isBoostAvailable());
        bonusModel.endContact(poneyModel);
        assert (poneyModel.isBoostAvailable());
        poneyModel.boost();
        assert (!poneyModel.isBoostAvailable());
        poneyModel.move(1);

        //Test : bonus utilisé et non visible
        assert (bonusModel.getUsed(poneyModel));
        assert (!bonusModel.isObservable(poneyModel));
    }
}
