package fr.univ_lyon1.groupen.poneymon.model;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class FieldModelTest {
    @Test(expected = IllegalArgumentException.class)
    public void testNegativePoneyCount() {
        List<ProtoPoney> protoPoneyList = new ArrayList<>();
        protoPoneyList.add(ProtoPoney.DEFAULTS[0]);
        protoPoneyList.add(ProtoPoney.DEFAULTS[2]);
        new FieldModel(new Race(100, Race.Difficulty.HARD,protoPoneyList));
    }

    @Test
    public void testCreatedPoneyCount() {
        List<ProtoPoney> protoPoneyList = new ArrayList<>();
        protoPoneyList.add(ProtoPoney.DEFAULTS[0]);
        protoPoneyList.add(ProtoPoney.DEFAULTS[2]);
        assertEquals(2, new FieldModel(new Race(100, Race.Difficulty.HARD, protoPoneyList)).getPoneys().length);
    }

    @Test
    public void testAllPoneysExist() {
        List<ProtoPoney> protoPoneyList = new ArrayList<>();
        protoPoneyList.add(ProtoPoney.DEFAULTS[0]);
        protoPoneyList.add(ProtoPoney.DEFAULTS[2]);
        protoPoneyList.add(ProtoPoney.DEFAULTS[3]);
        protoPoneyList.add(ProtoPoney.DEFAULTS[2]);
        FieldModel field = new FieldModel(new Race(100, Race.Difficulty.HARD, protoPoneyList));
        for (final PoneyModel poney : field.getPoneys()) {
            assertNotNull(poney);
        }
    }

    @Test
    public void testGetPoneyIndex() {
        List<ProtoPoney> protoPoneyList = new ArrayList<>();
        protoPoneyList.add(ProtoPoney.DEFAULTS[0]);
        protoPoneyList.add(ProtoPoney.DEFAULTS[2]);
        protoPoneyList.add(ProtoPoney.DEFAULTS[3]);
        FieldModel field = new FieldModel(new Race(100, Race.Difficulty.EASY, protoPoneyList));
        for (int i = 0; i < field.getPoneys().length; ++i) {
            assertEquals(field.getPoneys()[i], field.getPoney(i));
        }
    }
}
