package fr.univ_lyon1.groupen.poneymon.model;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class RaceModelTest {
    @Test
    public void creation(){
        List<ProtoPoney> protoPoneyList = new ArrayList<>();
        protoPoneyList.add(ProtoPoney.DEFAULTS[0]);
        protoPoneyList.add(ProtoPoney.DEFAULTS[2]);
        Race race = new Race(100, Race.Difficulty.HARD,protoPoneyList);
        assert (race.getItems().size() > 1);
        assert (race.getPoneyCount() == 2);
        assert (race.getAiControlledPoneys() == 0);
        assert (race.getRaceLength() == 100);
        race.setAiControlledPoneys(2);
        assert (race.getAiControlledPoneys() == 2);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testBadUse1() {
        List<ProtoPoney> protoPoneyList = new ArrayList<>();
        protoPoneyList.add(ProtoPoney.DEFAULTS[0]);
        protoPoneyList.add(ProtoPoney.DEFAULTS[2]);
        Race race = new Race(100, Race.Difficulty.HARD,protoPoneyList);
        new Race(-100, Race.Difficulty.EASY, protoPoneyList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testBadUseSettings(){
        List<ProtoPoney> protoPoneyList = new ArrayList<>();
        protoPoneyList.add(ProtoPoney.DEFAULTS[2]);
        protoPoneyList.add(ProtoPoney.DEFAULTS[1]);
        Race race = new Race(55, Race.Difficulty.HARD,protoPoneyList);
        race.setAiControlledPoneys(-2);
    }
}
