package fr.univ_lyon1.groupen.poneymon.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Unit test for the PoneyModel class.
 */
public class PoneyModelTest {
    /**
     * Simple example test for the PoneyModel class.
     */
    @Test
    public void testMoveSpeed() {
        // Given
        PoneyModel p = new PoneyModel(ProtoPoney.DEFAULTS[0], 1);
        p.speed = 0.42;
        p.x = 0;

        // When
        p.move(1);

        // Then
        //assertEquals(0.42, p.x, 0.001);
    }

    @Test
    public void testBoost() {
        // Given
        PoneyModel p = new PoneyModel(ProtoPoney.DEFAULTS[0], 0);
        p.speed = 0.42;
        p.x = 0;

        // When
        p.boost();

        // Then
        //assertEquals(0.84, p.speed, 0.001);
        //assertTrue(p.isBoost());
    }
}
