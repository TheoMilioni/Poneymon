# Poneymon

## Compilation et exécution

```
mvn compile
mvn exec:java
```

## Génération de la JavaDoc

```
mvn javadoc:javadoc
```
Une fois fait, ouvrir `target/site/apidocs/index.html`.

## Création d'un JAR *standalone*

```
mvn package
```
Un `jar-with-dependencies.jar` sera créé dans le dossier `target`. À lancer avec
`java -jar <chemin du jar>`.
